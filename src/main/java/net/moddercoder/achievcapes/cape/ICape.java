package net.moddercoder.achievcapes.cape;

import java.util.Map;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

public interface ICape {
	String getName();
	long getSerialNumber();
	EnumRarity getRarity();
	ResourceLocation getAdvancementID();
	Map<String, String> getTranslates();
}