package net.moddercoder.achievcapes.cape.config;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

public class Configs {
	
	public static Configuration config;
	
	//==================GENERAL==================
	public static boolean textureValidation;
	public static final String CONFIG_TEXTURE_VALIDATION_NAME = "textureValidation";
	//==========================================
	
	//==================CLIENT==================
	public static boolean guiAnimation;
	public static final String CONFIG_GUI_ANIMATION = "guiAnimation";
	public static boolean capeTextureCaching;
	public static final String CONFIG_CAPE_TEXTURE_CACHING = "capeTextureCaching";
	public static float guiCapeRotationSpeed;
	public static final String CONFIG_GUI_CAPE_ROTATION_SPEED = "guiCapeRotationSpeed";
	//==========================================
	
	public static void init(String configPath) {
		final File configFile = new File(configPath);
		
		Configs.config = new Configuration(configFile);
		
		//General
		Configs.textureValidation = Configs.config.getBoolean(Configs.CONFIG_TEXTURE_VALIDATION_NAME, Configuration.CATEGORY_GENERAL, false, "Validate texture URL for each cape. (Can make server load slow) (For DEDICATED SERVER that option is RECOMMENDED).");
		
		//Client
		Configs.guiAnimation = Configs.config.getBoolean(Configs.CONFIG_GUI_ANIMATION, Configuration.CATEGORY_CLIENT, true, "Slide animation for cape chooser GUI.");
		Configs.capeTextureCaching = Configs.config.getBoolean(Configs.CONFIG_CAPE_TEXTURE_CACHING, Configuration.CATEGORY_CLIENT, true, "Cache cape texture while connecting to the dedicated server. (Improves connecting time after first connection)");
		Configs.guiCapeRotationSpeed = Configs.config.getFloat(Configs.CONFIG_GUI_CAPE_ROTATION_SPEED, Configuration.CATEGORY_CLIENT, 12f, 0f, 24f, "Speed of cape rotation in cape chooser GUI.");
		
		Configs.config.save();
	}
}