package net.moddercoder.achievcapes.cape;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Collection;

public class CapeManager {
	
	private CapeManager () {}
	
	private List<ICape> capes = new ArrayList<>();
	
	public Optional<ICape> getCapeBySerialNumber (long serialNumber) {
		for (int i = 0; i < this.capes.size(); i ++)
			if (this.capes.get(i).getSerialNumber() == serialNumber)
				return Optional.of(this.capes.get(i));
		return Optional.empty();
	}
	
	public Collection<ICape> getAllCapesWithoutDeveloper () {
		final ArrayList<ICape> capes = new ArrayList<>();
		this.capes.forEach(capes::add);
		capes.removeIf((cape) -> cape.getSerialNumber() == CapesList.DEVELOPER_CAPE.getSerialNumber());
		
		return capes;
	}
	
	public Collection<ICape> getAllCapes () {
		return this.capes;
	}
	
	public boolean addCape (ICape cape) {
		if (this.capes.contains(cape))
			return false;
		
		return capes.add(cape);
	}
	
	public void clear() {
		this.capes.clear();
	}
	
	public static class Builder {
		public CapeManager build() {
			return new CapeManager();
		}
	}
	
}