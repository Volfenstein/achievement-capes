package net.moddercoder.achievcapes.cape.property.vars;

import net.moddercoder.achievcapes.cape.property.var.PropertyVarType;

public class ShortPropertyVarType extends PropertyVarType<Short> {
	public ShortPropertyVarType(String name, short defaultValue) {
		super(name, defaultValue);
	}
}