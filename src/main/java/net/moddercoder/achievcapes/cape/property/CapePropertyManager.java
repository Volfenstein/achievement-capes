package net.moddercoder.achievcapes.cape.property;

import java.util.function.Supplier;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.cape.property.var.PropertyVar;
import net.moddercoder.achievcapes.cape.property.var.PropertyVarType;

public class CapePropertyManager {
	
	public CapeProperty createProperty (ICape cape) {
		final CapeProperty property = new CapeProperty(cape);
		return property;
	}
	
	public CapeProperty createProperty (Supplier<ICape> supplier) {
		return this.createProperty(supplier.get());
	}
	
	public CapeProperty createEmptyProperty () {
		return this.createProperty(() -> null);
	}
	
	public CapeProperty ofProperty(CapeProperty property) {
		final CapeProperty propertyResult = new CapeProperty(property.getCapeSerialNumber());
		for (final PropertyVarType<?> type : CapeProperty.CapePropertySerializer.getAllCapePropertyVarTypes()) {
			propertyResult.varByName(new PropertyVar<>(type.getName(), property.varByName(type.getName()).getValue()));
		}
		return propertyResult;
	}
	
	public CapeProperty ofProperty(Supplier<CapeProperty> _property) {
		return this.ofProperty(_property.get());
	}
	
}