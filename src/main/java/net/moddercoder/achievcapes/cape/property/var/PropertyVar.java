package net.moddercoder.achievcapes.cape.property.var;

public class PropertyVar<T> {
	
	private T value;
	private String name;
	
	public PropertyVar(String name, T value) {
		this.value = value;
		this.name = name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
	public T getValue() {
		return this.value;
	}
}