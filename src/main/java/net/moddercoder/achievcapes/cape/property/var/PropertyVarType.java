package net.moddercoder.achievcapes.cape.property.var;

public class PropertyVarType<T> {
	
	private String name;
	private T defaultValue;
	
	public PropertyVarType(String name, T defaultValue) {
		this.name = name;
		this.defaultValue = defaultValue;
	}
	
	public T getDefaultVar() {
		return this.defaultValue;
	}
	
	public String getName() {
		return this.name;
	}
}