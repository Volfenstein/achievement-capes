package net.moddercoder.achievcapes.cape.property.vars;

import net.moddercoder.achievcapes.cape.property.var.PropertyVarType;

public class BytePropertyVarType extends PropertyVarType<Byte> {
	public BytePropertyVarType(String name, byte defaultValue) {
		super(name, defaultValue);
	}
}