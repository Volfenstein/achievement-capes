package net.moddercoder.achievcapes.cape.property.vars;

import net.moddercoder.achievcapes.cape.property.var.PropertyVarType;

public class IntPropertyVarType extends PropertyVarType<Integer> {
	public IntPropertyVarType(String name, int defaultValue) {
		super(name, defaultValue);
	}
}