package net.moddercoder.achievcapes.cape.property.vars;

import net.moddercoder.achievcapes.cape.property.var.PropertyVarType;

public class BooleanPropertyVarType extends PropertyVarType<Boolean> {
	public BooleanPropertyVarType(String name, boolean defaultValue) {
		super(name, defaultValue);
	}
}