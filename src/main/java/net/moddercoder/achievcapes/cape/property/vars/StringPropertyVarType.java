package net.moddercoder.achievcapes.cape.property.vars;

import net.moddercoder.achievcapes.cape.property.var.PropertyVarType;

public class StringPropertyVarType extends PropertyVarType<String> {
	public StringPropertyVarType(String name, String defaultValue) {
		super(name, defaultValue);
	}
}