package net.moddercoder.achievcapes.cape.property;

import java.util.ArrayList;
import java.util.Collection;

import io.netty.buffer.ByteBuf;

import java.util.function.Supplier;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.AchievementCapes;

import org.apache.commons.lang3.SerializationException;

import net.minecraftforge.fml.common.network.ByteBufUtils;

import net.moddercoder.achievcapes.cape.property.var.PropertyVar;
import net.moddercoder.achievcapes.cape.property.var.PropertyVarType;

public class CapeProperty {
	
	private long capeSerialNumber;
	private PropertyVar<?>[] vars;
	
	private CapePropertySerializer serializer;
	
	CapeProperty (ICape cape) {
		this.setCapeSerialNumberByCape(cape);
		this.serializer = new CapePropertySerializer(this);
		
		final int varCount = CapePropertySerializer.getAllCapePropertyVarTypes().size();
		this.vars = new PropertyVar<?>[varCount];
		
		for (int i = 0; i < varCount; i ++) {
			final PropertyVarType<?> type = CapePropertySerializer.varTypes.get(i);
			this.vars[i] = new PropertyVar<>(type.getName(), type.getDefaultVar());
		}
	}
	
	CapeProperty (Supplier<ICape> cape) {
		this(cape.get());
	}
	
	CapeProperty (long capeSerialNumber) {
		this.setCapeSerialNumber(capeSerialNumber);
		this.serializer = new CapePropertySerializer(this);
		
		int varCount = CapePropertySerializer.getAllCapePropertyVarTypes().size();
		this.vars = new PropertyVar<?>[varCount];
		
		for (int i = 0; i < varCount; i ++) {
			final PropertyVarType<?> type = CapePropertySerializer.varTypes.get(i);
			this.vars[i] = new PropertyVar<>(type.getName(), type.getDefaultVar());
		}
	}
	
	public CapeProperty varByName(String name, Object varValue) {
		boolean found = false;
		for (int i = 0; i < this.vars.length; i ++) {
			if (this.vars[i].getName().equals(name)) {
				this.vars[i] = new PropertyVar<>(name, varValue);
				found = true;
			}
		}
		if (found == false) {
			throw new IllegalStateException("Can't find PropertyVar by following name " + name);
		}
		return this;
	}
	
	public CapeProperty varByName(PropertyVar<?> var) {
		boolean found = false;
		for (int i = 0; i < this.vars.length; i ++) {
			if (this.vars[i].getName().equals(var.getName())) {
				this.vars[i] = var;
				found = true;
			}
		}
		if (found == false) {
			throw new IllegalStateException("Can't find PropertyVar by following name " + var.getName());
		}
		return this;
	}
	
	public CapePropertySerializer getSerializer() {
		return this.serializer;
	}
	
	public PropertyVar<?> varByName(String name) {
		for (final PropertyVar<?> var : vars) {
			if (var.getName().equals(name)) {
				return var;
			}
		}
		throw new IllegalStateException("Can't find PropertyVar by following name " + name);
	}
	
	public CapeProperty setCapeSerialNumber (long capeSerialNumber) {
		this.capeSerialNumber = capeSerialNumber;
		return this;
	}
	
	public CapeProperty setCapeSerialNumberByCape (ICape cape) {
		this.capeSerialNumber = cape == null ? -1L : cape.getSerialNumber();
		return this;
	}
	
	public CapeProperty setCapeSerialNumberByCape (Supplier<ICape> cape) {
		return this.setCapeSerialNumberByCape(cape.get());
	}
	
	public long getCapeSerialNumber () {
		return this.capeSerialNumber;
	}
	
	public boolean hasCape() {
		return this.capeSerialNumber != -1L;
	}
	
	public static class CapePropertySerializer {
		
		private static ArrayList<PropertyVarType<?>> varTypes = new ArrayList<PropertyVarType<?>>();
		
		public static Collection<PropertyVarType<?>> getAllCapePropertyVarTypes() {
			return CapePropertySerializer.varTypes;
		}
		
		public static boolean addCapePropertyVarType (PropertyVarType<?> varType) {
			if (CapePropertySerializer.varTypes.contains(varType) == false) {
				final boolean state = CapePropertySerializer.varTypes.add(varType);
				if (state) {
					AchievementCapes.logger.info(String.format("Succesfully added PropertyVarType for capes: [\"%s\"]", varType.getName()));
					return state;
				}
			}
			return false;
		}
		
		private CapeProperty property;
		
		private CapePropertySerializer(CapeProperty property) {
			this.property = property;
		}
		
		public synchronized void write(ByteBuf buffer) {
			buffer.writeLong(property.getCapeSerialNumber());
			for (final PropertyVarType<?> type : CapeProperty.CapePropertySerializer.getAllCapePropertyVarTypes()) {
				final PropertyVar<?> var = this.property.varByName(type.getName());
				ByteBufUtils.writeUTF8String(buffer, var.getName());
				
				final byte[] buf = this.serialize(var);
				buffer.writeInt(buf.length);
				buffer.writeBytes(buf);
			}
		}
		
		public static synchronized CapeProperty read(ByteBuf buffer) {
			final CapeProperty property = new CapeProperty(buffer.readLong());
			for (int i = 0; i < CapeProperty.CapePropertySerializer.getAllCapePropertyVarTypes().size(); i ++) {
				final String name = ByteBufUtils.readUTF8String(buffer);
				final ByteBuf readedBuf = buffer.readBytes(buffer.readInt());
				
				final byte[] buf = new byte[readedBuf.readableBytes()];
				final int readedIndex = readedBuf.readerIndex();
				readedBuf.getBytes(readedIndex, buf);
				
				final Object obj = CapePropertySerializer.deserialize(buf);
				final PropertyVar<?> var = new PropertyVar<>(name, obj);
				property.varByName(var);
			}
			return property;
		}
		
		private byte[] serialize (PropertyVar<?> var) {
			try {
				final ByteArrayOutputStream baos = new ByteArrayOutputStream();
				final ObjectOutputStream oos = new ObjectOutputStream(baos);
				oos.writeObject(var.getValue());
				oos.flush();
				
				return baos.toByteArray();
			} catch (SerializationException | IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		private static Object deserialize (byte[] buffer) {
			try {
				final ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
				final ObjectInputStream ois = new ObjectInputStream(bais);
				final Object obj = ois.readObject();
				
				ois.close();
				
				return obj;
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}