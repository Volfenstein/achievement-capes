package net.moddercoder.achievcapes;

import java.io.File;

import java.net.URL;

import java.util.UUID;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Optional;
import java.util.Collection;

import java.util.function.Supplier;
import java.util.function.Predicate;

import net.minecraft.util.ResourceLocation;

import java.util.concurrent.ConcurrentHashMap;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.entity.player.EntityPlayer;

import net.moddercoder.achievcapes.cape.ICape;
import net.moddercoder.achievcapes.cape.CapeManager;

import net.moddercoder.achievcapes.cape.property.CapeProperty;
import net.moddercoder.achievcapes.cape.property.CapePropertyManager;

//Horrible server and client delegation. I'm sorry :)
public interface IAchievementCapesCore {
	
	public boolean silentlyAddCapeForPlayer(Side side, UUID playerUUID, ICape cape);
	public boolean silentlyAddCapeForPlayer(Side side, EntityPlayer player, ICape cape);
	public boolean silentlyAddCapeForPlayerIf(Side side, Predicate<EntityPlayer> predicate, EntityPlayer player, ICape cape);
	
	public boolean removeCapeForPlayer(Side side, UUID playerUUID, ICape cape);
	public boolean removeCapeForPlayer(Side side, EntityPlayer player, ICape cape);
	public boolean removeCapeForPlayerIf(Side side, Predicate<UUID> predicate, UUID playerUUID, ICape cape);
	public boolean removeCapeForPlayerIf(Side side, Predicate<EntityPlayer> predicate, EntityPlayer player, ICape cape);
	
	public boolean addCapeForPlayer(Side side, EntityPlayer player, ICape cape);
	public boolean addCapeForPlayerIf(Side side, Predicate<EntityPlayer> predicate, EntityPlayer player, ICape cape);
	
	public CapeProperty getLastCapePropertyForPlayer(Side side, UUID playerUUID);
	public CapeProperty setLastCapePropertyForPlayer(Side side, UUID playerUUID, CapeProperty property);
	
	public CapePropertyManager getCapePropertyManager(Side side);
	public CapeProperty getCapePropertyFromPlayer(Side side, UUID playerUUID);
	public CapeProperty getCapePropertyFromPlayer(Side side, Supplier<UUID> supplier);
	public void setCapePropertyForPlayer(Side side, UUID playerUUID, CapeProperty property);
	public void setCapePropertyForPlayer(Side side, UUID playerUUID, Supplier<CapeProperty> supplier);
	
	public void initVisuals (Side side);
	public File getCapeVisualFile (Side side);
	public ConcurrentHashMap<Long, Optional<URL>> getVisualMap();
	//Make sense only on CLIENT
	public ConcurrentHashMap<Long, ResourceLocation> getClientVisualMap();
	public ConcurrentHashMap<Long, Optional<URL>> readVisualsFromFile (Side side);
	public void writeVisualsToFile (Side side, Map<Long, Optional<URL>> visualMap);
	
	public void initCapeData (Side side, HashMap<UUID, List<Long>> playersData);
	
	public Collection<ICape> getAvailableCapes(Side side, UUID playerUUID);
	public Collection<ICape> getAvailableCapes(Side side, EntityPlayer player);
	
	public void eraseCapeDataForPlayer(Side side, UUID playerUUID);
	public void eraseCapeDataForPlayer(Side side, EntityPlayer player);
	
	public void initCapeDataForPlayer(Side side, UUID playerUUID);
	public void initCapeDataForPlayer(Side side, EntityPlayer player);
	
	public int getAvailableCapesCount(Side side, UUID playerUUID);
	public int getAvailableCapesCount(Side side, EntityPlayer player);
	
	public Collection<UUID> getPlayersList(Side side);
	
	public CapeManager getCapeManager(Side side);
	
	public void resetAll(Side side);
}