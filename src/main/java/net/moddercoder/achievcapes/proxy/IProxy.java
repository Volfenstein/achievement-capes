package net.moddercoder.achievcapes.proxy;

import net.minecraft.item.Item;

import net.moddercoder.achievcapes.IAchievementCapesCore;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;

public interface IProxy {
	IAchievementCapesCore getCore();
	
	void preInit(FMLPreInitializationEvent event);
	void init(FMLInitializationEvent event);
	void postInit(FMLPostInitializationEvent event);
	
	void registerItemModel(Item item, int meta, String name);
}