package net.moddercoder.achievcapes.reference;

import java.util.UUID;

public class Reference {
	public static final String AUTHOR = "Avongroid";
	public static final String MODID = "achievcapes";
	public static final String VERSION = "1.1.1-beta";
    public static final String NAME = "Achievement Capes";
    
    //Don't hack me please :'(
    public static final UUID DEVELOPER_UUID = UUID.fromString("94eaeaf4-e175-480e-b1bc-0a0c26ceca89");
    
    public static final String CLIENT_PROXY = "net.moddercoder." + MODID + ".client.proxy.ClientProxy";
    public static final String SERVER_PROXY = "net.moddercoder." + MODID + ".server.proxy.ServerProxy";
}