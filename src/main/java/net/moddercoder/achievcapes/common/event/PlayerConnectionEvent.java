package net.moddercoder.achievcapes.common.event;

import java.util.Collection;

import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

import net.moddercoder.achievcapes.cape.ICape;
import net.moddercoder.achievcapes.cape.CapesList;

import net.moddercoder.achievcapes.reference.Reference;

import net.minecraft.util.text.TextComponentTranslation;

import net.moddercoder.achievcapes.AchievementCapes;
import net.moddercoder.achievcapes.IAchievementCapesCore;

import net.moddercoder.achievcapes.cape.property.CapeProperty;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import net.moddercoder.achievcapes.common.capedata.CommonCapeDataSaver;

import net.moddercoder.achievcapes.advancement.AchievementCapesAdvancement;

import net.moddercoder.achievcapes.common.network.PacketSendCapesList;
import net.moddercoder.achievcapes.common.network.PacketSendCapesVisuals;
import net.moddercoder.achievcapes.common.network.PacketSelectCapeValidate;
import net.moddercoder.achievcapes.common.network.PacketChangeCapeProperty;
import net.moddercoder.achievcapes.common.network.PacketInternetConnectionInfo;

import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;

import net.moddercoder.achievcapes.common.network.PacketSyncPlayerCapeProperties;

public class PlayerConnectionEvent {
	@SubscribeEvent
	public void onPlayerJoined(final PlayerLoggedInEvent event) {
		final EntityPlayer player = event.player;
		final IAchievementCapesCore core = AchievementCapes.getCore();
		
		final PacketInternetConnectionInfo packet = new PacketInternetConnectionInfo();
		AchievementCapes.getInstance().network.sendTo(packet, (EntityPlayerMP)player);
		
		if (AchievementCapes.hasInternetConnection == false) {
			final TextComponentTranslation hasntConnectionText = new TextComponentTranslation("text.hasntInternet.achievcapes.name");
			player.sendMessage(hasntConnectionText);
			return;
		}
		
		//Initialize cape property for other players on newly connected player [FIXED: NullPointerException while cape rendering]
		final PacketSelectCapeValidate otherPlayersInitCapePropertyForThatPlayerPacket = new PacketSelectCapeValidate(player.getUniqueID(), -1L, true);
		AchievementCapes.getInstance().network.sendToAll(otherPlayersInitCapePropertyForThatPlayerPacket);
		//
		
		final World world = player.getEntityWorld();
		
		final PacketSendCapesList packetSendCapesList = new PacketSendCapesList(core.getCapeManager(Side.SERVER).getAllCapes().toArray(new ICape[0]));
		final PacketSendCapesVisuals packetSendCapesVisuals = new PacketSendCapesVisuals(core.getVisualMap());
		
		AchievementCapes.getInstance().network.sendTo(packetSendCapesList, (EntityPlayerMP)player);
		AchievementCapes.logger.info("Capes list sended to client : " + player.toString() + " with uuid : " + player.getUniqueID());
		AchievementCapes.getInstance().network.sendTo(packetSendCapesVisuals, (EntityPlayerMP)player);
		AchievementCapes.logger.info("Capes visuals sended to client : " + player.toString() + " with uuid : " + player.getUniqueID());
		
		//Given capes
		final Collection<ICape> playerAvailableCapes = core.getAvailableCapes(Side.SERVER, player.getUniqueID());
		if (playerAvailableCapes != null) {
			playerAvailableCapes.forEach((c) -> core.silentlyAddCapeForPlayer(Side.SERVER, player, c));
			core.addCapeForPlayerIf(Side.SERVER, (p) -> p.getUniqueID().equals(Reference.DEVELOPER_UUID) && !playerAvailableCapes.contains(CapesList.DEVELOPER_CAPE), player, CapesList.DEVELOPER_CAPE);
		} else {
			core.initCapeDataForPlayer(Side.SERVER, player.getUniqueID());
			core.addCapeForPlayerIf(Side.SERVER, (p) -> p.getUniqueID().equals(Reference.DEVELOPER_UUID), player, CapesList.DEVELOPER_CAPE);
		}
		//
		
		//Last cape
		final CapeProperty lastProperty = core.getCapePropertyFromPlayer(Side.SERVER, player::getUniqueID);
		if (lastProperty != null) {
			core.setCapePropertyForPlayer(Side.SERVER, player.getUniqueID(), lastProperty);
			
			final PacketChangeCapeProperty packetChangeCapeProperty = new PacketChangeCapeProperty(player.getUniqueID(), lastProperty);
			AchievementCapes.getInstance().network.sendToAll(packetChangeCapeProperty);
		} else {
			core.setCapePropertyForPlayer(Side.SERVER, player.getUniqueID(), core.getCapePropertyManager(Side.SERVER).createEmptyProperty());
			
			final PacketChangeCapeProperty packetChangeCapeProperty = new PacketChangeCapeProperty(player.getUniqueID(), core.getCapePropertyManager(Side.SERVER).createEmptyProperty());
			AchievementCapes.getInstance().network.sendToAll(packetChangeCapeProperty);
		}
		
		//Sync capes
		final PacketSyncPlayerCapeProperties packetSyncPlayers = new PacketSyncPlayerCapeProperties();
		AchievementCapes.getInstance().network.sendTo(packetSyncPlayers, (EntityPlayerMP)player);
		//
		
		if (world instanceof WorldServer && player instanceof EntityPlayerMP) {
			AchievementCapesAdvancement.checkForAdvancements((WorldServer)world, (EntityPlayerMP)player);
		}
	}
	
	@SubscribeEvent
	public void onPlayerLeaved(final PlayerLoggedOutEvent event) {
		final EntityPlayer player = event.player;
		CommonCapeDataSaver.savePlayerData(player);
		
		final CapeProperty lastProperty = AchievementCapes.getCore().getCapePropertyFromPlayer(Side.SERVER, player::getUniqueID);
		if (lastProperty != null) {
			AchievementCapes.getCore().setLastCapePropertyForPlayer(Side.SERVER, player.getUniqueID(), lastProperty);
		}
	}
}