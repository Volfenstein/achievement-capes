package net.moddercoder.achievcapes.common.capedata;

import java.util.List;
import java.util.UUID;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collection;

import java.lang.reflect.Type;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;

import com.google.gson.reflect.TypeToken;

import net.moddercoder.achievcapes.cape.ICape;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.entity.player.EntityPlayer;

import net.minecraftforge.fml.client.FMLClientHandler;

import net.minecraftforge.fml.common.FMLCommonHandler;

import net.moddercoder.achievcapes.reference.Reference;

import net.moddercoder.achievcapes.AchievementCapes;
import net.moddercoder.achievcapes.IAchievementCapesCore;

public class CommonCapeDataSaver {
	
	private static final String FILE_NAME = "players";
	private static final String FILE_EXTENSION = ".json";
	private static final String FOLDER_NAME = Reference.MODID.concat("_data");
	
	public static boolean isConfigExists () {
		final File folderFile = new File(CommonCapeDataSaver.getSaveDir().getAbsolutePath());
		final File configFile = new File(CommonCapeDataSaver.getSaveFile().getAbsolutePath());
		if (!folderFile.exists() || !configFile.exists())
			return false;
		return true;
	}
	
	public static HashMap<UUID, List<Long>> readData() {
		if (!CommonCapeDataSaver.isConfigExists()) 
			return null;
		
		final File configFile = CommonCapeDataSaver.getSaveFile();
		
		String jsonData = "";
		
		try {
			final BufferedReader reader = new BufferedReader(new FileReader(configFile));
			String line = "";
			
			while ((line=reader.readLine()) != null) {
				jsonData += line;
			}
			
			reader.close();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		final Type type = new TypeToken<HashMap<UUID, List<Long>>>(){}.getType();
		HashMap<UUID, List<Long>> playersData = AchievementCapes.GSON.<HashMap<UUID, List<Long>>>fromJson(jsonData, type);
		playersData = playersData == null ? new HashMap<>() : playersData;
		return playersData;
	}
	
	public static void savePlayerData (EntityPlayer player) {
		final UUID playerUUID = player.getUniqueID();
    	
    	final Collection<ICape> playerCapes = AchievementCapes.getCore().getAvailableCapes(Side.SERVER, playerUUID);
    	if (playerCapes == null) return;
		
    	final List<Long> serialList = new ArrayList<>();
    	HashMap<UUID, List<Long>> players = CommonCapeDataSaver.readData();
    	if (players == null) {
    		players = new HashMap<>();
    		players.put(playerUUID, serialList);
    	}
    	
    	final HashMap<UUID, List<Long>> editablePlayers = new HashMap<>();
    	players.forEach(editablePlayers::put);
    	
    	playerCapes.forEach((cape) -> {
    		if (cape != null) {
    			serialList.add(cape.getSerialNumber());
    		}
		});
    	
    	players.keySet().forEach((uuid) -> {
    		if (uuid == playerUUID) {
    			editablePlayers.remove(playerUUID);
    			editablePlayers.put(playerUUID, serialList);
    		} else {
    			editablePlayers.put(uuid, editablePlayers.get(uuid));
    		}
    	});
    	
    	CommonCapeDataSaver.writeToFile(AchievementCapes.GSON.toJson(editablePlayers));
	}
	
	public static void saveAllPlayersData () {
		final IAchievementCapesCore core = AchievementCapes.getCore();
		
    	final HashMap<UUID, List<Long>> playersData = new HashMap<>();
    	
    	core.getPlayersList(Side.SERVER).forEach((uuid) -> {
    		final List<Long> serialList = new ArrayList<Long>();
    		final Collection<ICape> availableCapes = core.getAvailableCapes(Side.SERVER, uuid);
    		
    		availableCapes.forEach((cape) -> {
				if (cape != null) {
					serialList.add(cape.getSerialNumber());
				}
			});
    		
    		playersData.put(uuid, serialList);
    	});
    	
    	CommonCapeDataSaver.writeToFile(AchievementCapes.GSON.toJson(playersData));
	}
	
	public static void writeToFile(String data) {
		try {
			final File fileFolder = CommonCapeDataSaver.getSaveDir();
			if (!fileFolder.exists() & !fileFolder.mkdir()) {
				AchievementCapes.logger.warn("Unable to create dir: " + fileFolder.getAbsoluteFile());
			}
			final FileWriter writer = new FileWriter(CommonCapeDataSaver.getSaveFile());
			writer.write(data);
			writer.flush();
			writer.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public static File getSaveFile () {
		final File file = new File(CommonCapeDataSaver.getSaveDir().getAbsolutePath().concat("/" + FILE_NAME.concat(FILE_EXTENSION)));
		return file;
	}
	
	public static File getSaveDir () {
		final FMLCommonHandler commonHandler = FMLCommonHandler.instance();
		if (commonHandler.getSide() == Side.CLIENT) {
			final FMLClientHandler clientHandler = FMLClientHandler.instance();
			final File folderFile = new File(clientHandler.getSavesDir().getAbsolutePath());
			return new File(folderFile.getAbsolutePath().concat("/" + clientHandler.getServer().getFolderName() + "/" + FOLDER_NAME));
		} else {
			final File folderFile = new File(commonHandler.getSavesDirectory().getAbsolutePath());
			return new File(folderFile.getAbsolutePath().concat("/" + commonHandler.getMinecraftServerInstance().getFolderName() + "/" + FOLDER_NAME));
		}
	}
}