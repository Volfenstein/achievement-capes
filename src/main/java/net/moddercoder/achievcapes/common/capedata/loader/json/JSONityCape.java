package net.moddercoder.achievcapes.common.capedata.loader.json;

import java.util.Map;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.util.RarityUtil;

public class JSONityCape {
	
	//Required
	public String name;
	public String rarity;
	public long serialNumber;
	public Map<String, String> translate;
	
	//Optional
	public String advancementID = "";
	public String dependentendModID = "";
	
	public JSONityCape (String name, long serialNumber, String rarity, Map<String, String> translate, String advancementID, String dependentendModID) {
		this.name = name;
		this.translate = translate;
		this.serialNumber = serialNumber;
		this.rarity = rarity.toUpperCase();
		this.advancementID = advancementID;
		this.dependentendModID = dependentendModID;
	}
	
	public static JSONityCape from(ICape cape) {
		final ResourceLocation rAdvancementID = cape.getAdvancementID();
		final String advancementID = rAdvancementID != null ? rAdvancementID.getResourceDomain().concat(":" + rAdvancementID.getResourcePath()) : "";
		return new JSONityCape(cape.getName(), cape.getSerialNumber(), cape.getRarity().rarityName, cape.getTranslates(), advancementID, "");
	}
	
	public static ICape to(JSONityCape cape) {
		return new ICape() {
			@Override
			public String getName() {
				return cape.name;
			}
			@Override
			public long getSerialNumber() {
				return cape.serialNumber;
			}
			@Override
			public EnumRarity getRarity() {
				return RarityUtil.getRarityByName(cape.rarity);
			}
			@Override
			public Map<String, String> getTranslates() {
				return cape.translate;
			}
			@Override
			public ResourceLocation getAdvancementID() {
				if (cape.advancementID != null) {
					if (!cape.advancementID.isEmpty()) {
						final String[] _advancementID = cape.advancementID.split(":");
						final String resourceDomain = _advancementID[0] != null ? _advancementID[0] : "minecraft";
						final String resourcePath = _advancementID[1] != null ? _advancementID[1] : "story/mine_stone";
						
						return new ResourceLocation(resourceDomain, resourcePath);
					}
				}
				return null;
			}
		};
	}
	
	public JSONityCape fromICape (ICape cape) {
		return JSONityCape.from(cape);
	}
	
	@Override
	public String toString () {
		return "[Name:" + name + ", Rarity:" + rarity + ", serial:" + serialNumber + ", dependentModID:" + dependentendModID + ", advancementID:" + advancementID + "]";
	}
	
	public ICape toICape () {
		return JSONityCape.to(this);
	}
}