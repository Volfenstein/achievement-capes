package net.moddercoder.achievcapes.common.capedata.loader;

import java.net.URL;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.Objects;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;

import org.apache.logging.log4j.Level;

import com.google.gson.JsonSyntaxException;

import com.google.common.collect.ImmutableMap;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.common.cape.CommonCapesList;

import net.moddercoder.achievcapes.common.capedata.CommonCapeDataSaver;

import net.moddercoder.achievcapes.common.capedata.loader.json.JSONityCape;

public class CapeLoader {
	
	public static boolean firstRun = false;
	
	public static final String DATA_NAME = "data";
	public static final String DATA_EXTENSION = ".json";
	
	public static JSONityCape[] loadCapes () {
		if (CapeLoader.isConfigExists()) {
			final List<File> files = Arrays.<File>asList(CapeLoader.getCapesDir().listFiles());
			
			if (files.size() < 1) {
				CapeLoader.writeDefaultCapes();
				return CapeLoader.loadCapes();
			}
			
			final ArrayList<File> folders = new ArrayList<>();
			folders.addAll(files);
			folders.removeIf((folder) -> !folder.isDirectory());
			
			final ArrayList<JSONityCape> jsonityCapes = new ArrayList<>();
			
			folders.forEach((folder) -> {
				final File capeDataFile = new File(folder.getAbsolutePath().concat(File.separator) + DATA_NAME + DATA_EXTENSION);
				
				String jsonData = "";
				
				try {					
					final BufferedReader reader = new BufferedReader(new FileReader(capeDataFile));
					
					String line = "";
					
					while ((line=reader.readLine())!=null) {
						jsonData += line;
					}
					
					reader.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				try {
					final JSONityCape cape = AchievementCapes.GSON.<JSONityCape>fromJson(jsonData, JSONityCape.class);
					jsonityCapes.add(cape);
				} catch (final JsonSyntaxException jse) {
					AchievementCapes.logger.error(String.format("Inside file \"%s\" found a json syntax error:\n===\n%s\n===", capeDataFile, jse.getMessage()));
				}
			});
			
			AchievementCapes.logger.log(Level.INFO, "Loaded next capes...");
			
			final JSONityCape[] jsonityCapesArray = jsonityCapes.stream().filter(Objects::nonNull).toArray(JSONityCape[]::new);
			for (final JSONityCape jsonityCape : jsonityCapesArray) {
				AchievementCapes.logger.log(Level.INFO, jsonityCape.toString());
			}
			
			return jsonityCapesArray;
		}
		
		CapeLoader.writeDefaultCapes();
		return CapeLoader.loadCapes();
	}
	
	public static final Map<Long, String> defaultCapesTextures = new ImmutableMap.Builder<Long, String>()
		.put(CommonCapesList.ENDERMAN_CAPE.getSerialNumber(), "https://i.imgur.com/jm0GkOW.png")
		.put(CommonCapesList.VETERAN_CAPE.getSerialNumber(), "https://i.imgur.com/ZMwn4oI.png")
		.put(CommonCapesList.RUIN_CAPE.getSerialNumber(), "https://i.imgur.com/6AhEL74.png")
		.put(CommonCapesList.STAR_CAPE.getSerialNumber(), "https://i.imgur.com/6l75YLc.png")
		.put(CommonCapesList.MARKETPLACE_CAPE.getSerialNumber(), "https://i.imgur.com/KKShzTT.png")
		.put(CommonCapesList.FLAME_CAPE.getSerialNumber(), "https://i.imgur.com/HVyVdBl.png")
		.put(CommonCapesList.SPADES_CAPE.getSerialNumber(), "https://i.imgur.com/nLM1QEb.png")
		.put(CommonCapesList.MOJANG_CAPE.getSerialNumber(), "https://i.imgur.com/NzEBtdF.png")
		.put(CommonCapesList.PAN_CAPE.getSerialNumber(), "https://i.imgur.com/Ej5Dp3B.png")
		.put(CommonCapesList.SCROLLS_CAPE.getSerialNumber(), "https://i.imgur.com/55Hycuq.png")
		.put(CommonCapesList.VEGETATION_CAPE.getSerialNumber(), "https://i.imgur.com/t0X99VJ.png")
		.put(CommonCapesList.SNOWMAN_CAPE.getSerialNumber(), "https://i.imgur.com/XpaM2Vd.png")
		.put(CommonCapesList.GREEN_CAPE.getSerialNumber(), "https://i.imgur.com/D1YxgaZ.png")
	.build();
	
	public static void writeDefaultCapes () {
		CapeLoader.firstRun = true;
		
		CapeLoader.writeToFile(CommonCapesList.ENDERMAN_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.ENDERMAN_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.VETERAN_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.VETERAN_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.RUIN_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.RUIN_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.STAR_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.STAR_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.MARKETPLACE_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.MARKETPLACE_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.FLAME_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.FLAME_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.SPADES_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.SPADES_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.MOJANG_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.MOJANG_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.PAN_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.PAN_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.SCROLLS_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.SCROLLS_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.VEGETATION_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.VEGETATION_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.SNOWMAN_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.SNOWMAN_CAPE.getSerialNumber()));
		CapeLoader.writeToFile(CommonCapesList.GREEN_CAPE, CapeLoader.defaultCapesTextures.get(CommonCapesList.GREEN_CAPE.getSerialNumber()));
		
		AchievementCapes.logger.info("Created a default capes... Caused By: Haven't found a cape configs.");
	}
	
	public static void writeToFile(ICape cape, String textureURL) {
		try {
			final File capeFolder = new File(CapeLoader.getCapesDir().getAbsolutePath().concat("/"), cape.getName());
			if (!CommonCapeDataSaver.getSaveDir().exists() & !CommonCapeDataSaver.getSaveDir().mkdir()) {
				AchievementCapes.logger.error("Unable to write data: " + CommonCapeDataSaver.getSaveDir().getAbsoluteFile() + ", for cape: \"" + cape.getName() + "\"");
			}
			if (!capeFolder.exists() & !capeFolder.mkdirs()) {
				AchievementCapes.logger.error("Unable to create a folder: " + capeFolder.getAbsoluteFile() + ", for cape: \"" + cape.getName() + "\"");
			}
			
			final FileWriter writer = new FileWriter(new File(capeFolder.getAbsolutePath().concat("/") + (DATA_NAME.concat(DATA_EXTENSION))));
			final String DATA = AchievementCapes.GSON.toJson(JSONityCape.from(cape));
			
			final BufferedImage capeTexture = AchievementCapes.IMGUR_MANAGER.downloadImage(new URL(textureURL));
			ImageIO.write(capeTexture, "png", new File(capeFolder.getAbsolutePath(), "/cape.png"));
			
			writer.write(DATA);
			writer.flush();
			writer.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public static BufferedImage readCapeTexture(ICape cape) {
		try {
			return ImageIO.read(CapeLoader.getCapeTextureFile(cape));
		} catch (Exception e) {
			return new BufferedImage(22, 17, BufferedImage.TYPE_INT_RGB);
		}
	}
	
	public static boolean isCapeTextureExists(ICape cape) {
		return CapeLoader.getCapeTextureFile(cape).exists();
	}
	
	public static File getCapeTextureFile(ICape cape) {
		return new File(CapeLoader.getCapesDir().getAbsolutePath(), "/" + cape.getName() + "/cape.png");
	}
	
	public static boolean isConfigExists() {
		final File folder = CapeLoader.getCapesDir();
		return folder.isDirectory() && folder.exists();
	}
	
	public static File getCapesDir() {
		return new File(CommonCapeDataSaver.getSaveDir().getAbsolutePath().concat("/capes"));
	}
	
}