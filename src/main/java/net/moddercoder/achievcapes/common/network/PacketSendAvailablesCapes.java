package net.moddercoder.achievcapes.common.network;

import java.util.Optional;

import io.netty.buffer.ByteBuf;

import net.minecraft.client.Minecraft;

import net.minecraftforge.fml.relauncher.Side;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.fml.common.FMLCommonHandler;

import net.moddercoder.achievcapes.client.gui.GuiCapeChooserScreen;
import net.moddercoder.achievcapes.client.gui.GuiCapeChooserScreenDebug;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketSendAvailablesCapes implements IMessage, IMessageHandler<PacketSendAvailablesCapes, IMessage> {
	
	private byte inv;
	private long[] serialNumbers;
	
	public PacketSendAvailablesCapes() {}
	
	public PacketSendAvailablesCapes (long ... serialNumbers) {
		this.serialNumbers = serialNumbers;
	}
	
	public PacketSendAvailablesCapes (byte inv, long ... serialNumbers) {
		this.inv = inv;
		this.serialNumbers = serialNumbers;
	}
	
	@Override
	public IMessage onMessage(PacketSendAvailablesCapes message, MessageContext ctx) {
		final long[] availableSerialNumbers = message.serialNumbers;
		final byte inv = message.inv;
		
		Minecraft.getMinecraft().addScheduledTask(() -> {
			final ICape[] availableCapes = new ICape[availableSerialNumbers.length];
			int col = 0;
			
			for (final long serialNumber : availableSerialNumbers) {
				final Optional<ICape> cape = AchievementCapes.getCore().getCapeManager(Side.CLIENT).getCapeBySerialNumber(serialNumber);
				if (!cape.isPresent()) {
					AchievementCapes.logger.error("Client can't find a cape with serialNumber:" + serialNumber);
					continue;
				}
				
				availableCapes[col++] = cape.get();
			}
			
			if (inv == (byte)0) {
				FMLCommonHandler.instance().showGuiScreen(new GuiCapeChooserScreen(availableCapes));
			} else {
				FMLCommonHandler.instance().showGuiScreen(new GuiCapeChooserScreenDebug(availableCapes));
			}
		});
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.inv = buf.readByte();
		
		int serialCounts = buf.readInt();
		this.serialNumbers = new long[serialCounts];
		
		for (int i = 0; i < serialCounts; i ++) {
			this.serialNumbers[i] = buf.readLong();
		}
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeByte(this.inv);
		
		buf.writeInt(this.serialNumbers.length);
		for (final long serialNumber : this.serialNumbers) {
			buf.writeLong(serialNumber);
		}
	}
	
}