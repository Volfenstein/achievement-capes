package net.moddercoder.achievcapes.common.network;

import java.util.UUID;
import java.util.Optional;

import io.netty.buffer.ByteBuf;

import net.minecraft.client.Minecraft;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.client.entity.EntityPlayerSP;

import net.moddercoder.achievcapes.util.RarityUtil;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.cape.ICape;
import net.moddercoder.achievcapes.cape.CapeManager;

import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketCapeAlertMessage implements IMessage, IMessageHandler<PacketCapeAlertMessage, PacketCapeAlertMessage> {
	
	private UUID playerUUID;
	private long serialNumber;
	
	public PacketCapeAlertMessage () {}
	
	public PacketCapeAlertMessage (UUID playerUUID, long serialNumber) {
		this.playerUUID = playerUUID;
		this.serialNumber = serialNumber;
	}
	
	@Override
	public PacketCapeAlertMessage onMessage(PacketCapeAlertMessage message, MessageContext ctx) {
		final UUID playerUUID = message.playerUUID;
		final long serialNumber = message.serialNumber;
		
		final Minecraft minecraft = Minecraft.getMinecraft();
		
		minecraft.addScheduledTask(() -> {
			final CapeManager clientCapeManager = AchievementCapes.getCore().getCapeManager(Side.CLIENT);
			final String langCode = minecraft.getLanguageManager().getCurrentLanguage().getLanguageCode();
			
			final Optional<ICape> cape = clientCapeManager.getCapeBySerialNumber(serialNumber);
			if (cape.isPresent()) {
				this.showMessage(playerUUID, minecraft.player, cape.get(), langCode);
			} else {
				AchievementCapes.logger.error("Client hasn't a cape with serialNumber:" + serialNumber);
				AchievementCapes.logger.error("Client hasn't a cape with serialNumber:" + serialNumber);
				AchievementCapes.logger.error("Client hasn't a cape with serialNumber:" + serialNumber);
			}
		});
		
		return null;
	}
	
	private void showMessage (UUID playerUUID, EntityPlayerSP player, ICape cape, String langCode) {
		final TextComponentTranslation t1 = new TextComponentTranslation("text.player_got.achievcapes.name");
		final TextComponentTranslation t2 = new TextComponentTranslation("text.enterValue.achievcapes.name", player.world.getPlayerEntityByUUID(playerUUID).getName());
		t2.setStyle(new Style().setColor(TextFormatting.GOLD));
		final TextComponentTranslation t3 = new TextComponentTranslation("text.player_got2.achievcapes.name");
		final TextComponentTranslation t4 = new TextComponentTranslation("text.enterCape.achievcapes.name", cape.getTranslates().get(cape.getTranslates().containsKey(langCode) ? langCode : "en_us"));
		t4.setStyle(new Style().setColor(RarityUtil.getFormattingByRarity(cape.getRarity())));
		final TextComponentTranslation t5 = new TextComponentTranslation("text.exclamationPoint.achievcapes.name");
		final TextComponentTranslation t6 = new TextComponentTranslation("text.player_got_hasntInternet.achievcapes.name");
		
		final TextComponentString result = new TextComponentString(t1.getFormattedText() + " " + t2.getFormattedText() + " " + t3.getFormattedText() + " " + t4.getFormattedText() + " " + t5.getFormattedText());
		result.appendText(AchievementCapes.hasInternetConnection == false ? " " + t6.getFormattedText() : "");
		
		player.sendMessage(result);
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.serialNumber = buf.readLong();
		this.playerUUID = new UUID(buf.readLong(), buf.readLong());
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(this.serialNumber);
		buf.writeLong(this.playerUUID.getMostSignificantBits());
		buf.writeLong(this.playerUUID.getLeastSignificantBits());
	}
}
