package net.moddercoder.achievcapes.common.network;

import io.netty.buffer.ByteBuf;

import net.minecraft.client.Minecraft;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketInternetConnectionInfo implements IMessage, IMessageHandler<PacketInternetConnectionInfo, PacketInternetConnectionInfo> {
	
	private boolean connected = AchievementCapes.hasInternetConnection;
	
	public PacketInternetConnectionInfo () {}
	
	public PacketInternetConnectionInfo (boolean connected) {
		this.connected = connected;
	}
	
	@Override
	public PacketInternetConnectionInfo onMessage(PacketInternetConnectionInfo message, MessageContext ctx) {
		final boolean connected = message.connected;
		
		Minecraft.getMinecraft().addScheduledTask(() -> AchievementCapes.hasInternetConnection = connected);
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.connected = buf.readBoolean();
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeBoolean(this.connected);
	}
}
