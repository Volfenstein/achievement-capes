package net.moddercoder.achievcapes.common.network;

import java.util.UUID;

import io.netty.buffer.ByteBuf;

import net.minecraft.client.Minecraft;

import net.minecraftforge.fml.relauncher.Side;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.cape.property.CapeProperty;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketChangeCapeProperty implements IMessage, IMessageHandler<PacketChangeCapeProperty, IMessage> {
	
	private UUID playerUUID;
	private CapeProperty property;
	
	public PacketChangeCapeProperty() {}
	
	public PacketChangeCapeProperty(UUID playerUUID, CapeProperty property) {
		this.property = property;
		this.playerUUID = playerUUID;
	}
	
	@Override
	public IMessage onMessage(PacketChangeCapeProperty message, MessageContext ctx) {
		final UUID playerUUID = message.playerUUID;
		final CapeProperty propertyNew = message.property;
		
		Minecraft.getMinecraft().addScheduledTask(() ->
			AchievementCapes.getCore().setCapePropertyForPlayer(Side.CLIENT, playerUUID, AchievementCapes.getCore().getCapePropertyManager(Side.CLIENT).ofProperty(propertyNew))
		);
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.playerUUID = new UUID(buf.readLong(), buf.readLong());
		this.property = CapeProperty.CapePropertySerializer.read(buf);
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(this.playerUUID.getMostSignificantBits());
		buf.writeLong(this.playerUUID.getLeastSignificantBits());
		this.property.getSerializer().write(buf);
	}
	
}