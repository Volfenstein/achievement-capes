package net.moddercoder.achievcapes.common.network;

import java.util.Map;
import java.util.HashMap;
import java.util.Optional;

import java.util.Iterator;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import io.netty.buffer.ByteBuf;

import java.awt.image.BufferedImage;

import java.net.URL;
import java.net.MalformedURLException;

import net.minecraft.client.Minecraft;

import net.minecraft.util.ResourceLocation;

import net.minecraftforge.fml.relauncher.Side;

import net.moddercoder.achievcapes.cape.ICape;
import net.moddercoder.achievcapes.cape.CapesList;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.cape.config.Configs;

import net.moddercoder.achievcapes.util.ResourceLocations;

import net.minecraftforge.fml.common.network.ByteBufUtils;

import net.moddercoder.achievcapes.client.model.CapeModel;
import net.moddercoder.achievcapes.client.model.CapeModels;

import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.TextureManager;

import net.moddercoder.achievcapes.client.cache.CapeVisualCache;

import net.moddercoder.achievcapes.common.capedata.loader.CapeLoader;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketSendCapesVisuals implements IMessage, IMessageHandler<PacketSendCapesVisuals, IMessage> {
	
	private Map<Long, Optional<URL>> visualMap = new HashMap<Long, Optional<URL>>();
	
	public PacketSendCapesVisuals() {}
	
	public PacketSendCapesVisuals(Map<Long, Optional<URL>> visualMap) {
		this.visualMap = visualMap;
	}
	
	@Override
	public IMessage onMessage(PacketSendCapesVisuals message, MessageContext ctx) {
		final HashMap<Long, Optional<URL>> visualMap = (HashMap<Long, Optional<URL>>)message.visualMap;
		
		final Minecraft minecraftInstance = Minecraft.getMinecraft();
		
		minecraftInstance.addScheduledTask(() -> {
			final boolean singleplayer = minecraftInstance.isSingleplayer();
			
			if (!singleplayer) {
				CapeVisualCache.loadCachedVisualMap(ctx.getClientHandler().getNetworkManager().getRemoteAddress().toString());
			}
			
			final TextureManager manager = minecraftInstance.getTextureManager();
			final Iterator<ICape> iterator = AchievementCapes.getCore().getCapeManager(Side.CLIENT).getAllCapes().iterator();
			while (iterator.hasNext()) {
				final ICape cape = iterator.next();
				if (!AchievementCapes.getCore().getClientVisualMap().containsKey(cape.getSerialNumber())) {
					ResourceLocation texture = null;
					
					if (cape.getSerialNumber() == CapesList.DEVELOPER_CAPE.getSerialNumber()) {
						texture = ResourceLocations.ofCapeTexture(cape.getName());
					} else {
						if (!visualMap.containsKey(cape.getSerialNumber())) {
							AchievementCapes.logger.warn("Client visual map haven't data for: " + cape.getSerialNumber());
							continue;
						}
						if (!visualMap.get(cape.getSerialNumber()).isPresent()) {
							AchievementCapes.logger.warn("Client visual map haven't data for: " + cape.getSerialNumber());
							continue;
						}
						
						if (!singleplayer) {
							final URL textureURL = visualMap.get(cape.getSerialNumber()).get();
							
							final BufferedImage capeImage = AchievementCapes.IMGUR_MANAGER.downloadImage(textureURL);
							final DynamicTexture capeTexture = new DynamicTexture(capeImage);
							texture = manager.getDynamicTextureLocation(String.valueOf(cape.getSerialNumber()), capeTexture);
							
							AchievementCapes.logger.info(String.format("Downloaded texture for cape \"%s\": [\"%s\"]", cape.getName(), textureURL.toString()));
							
							if (Configs.capeTextureCaching) {
								CapeVisualCache.addToQueue(cape.getSerialNumber(), capeImage);
							}
						} else {
							try {
								final File capeTextureImageFile = CapeLoader.getCapeTextureFile(cape);
								
								final BufferedImage capeImage = ImageIO.read(capeTextureImageFile);
								final DynamicTexture capeTexture = new DynamicTexture(capeImage);
								texture = manager.getDynamicTextureLocation(String.valueOf(cape.getSerialNumber()), capeTexture);
								
								//AchievementCapes.logger.info(String.format("Loaded texture for cape \"%s\": [\"%s\"]", cape.getName(), capeTextureImageFile.getPath()));
							} catch (final IOException ioe) {
								ioe.printStackTrace();
							}
						}
					}
					AchievementCapes.getCore().getClientVisualMap().put(cape.getSerialNumber(), texture);
				}
				
				final CapeModel modelCape = new CapeModel(cape);
				CapeModels.models.put(cape.getSerialNumber(), Optional.of(modelCape));
			}
			
			if (Configs.capeTextureCaching & !singleplayer) {
				CapeVisualCache.cache(ctx.getClientHandler().getNetworkManager().getRemoteAddress().toString());
			}
		});
		
		return null;
	}
	
	@Override
	public void fromBytes(final ByteBuf buf) {
		final int size = buf.readInt();
		for (int i = 0; i < size; i ++) {
			final long serialNumber = buf.readLong();
			final String sURL = ByteBufUtils.readUTF8String(buf);
			try {
				final URL url = new URL(sURL);
				this.visualMap.put(serialNumber, Optional.of(url));
			} catch (final MalformedURLException e) {
				final Optional<ICape> cape = AchievementCapes.getCore().getCapeManager(Side.CLIENT).getCapeBySerialNumber(serialNumber);
				if (!cape.isPresent()) {
					AchievementCapes.logger.error("Client can't found cape with serialNumber: " + serialNumber);
				}
				
				AchievementCapes.logger.error(sURL + " is malformed!");
				continue;
			}
		}
	}
	
	@Override
	public void toBytes(final ByteBuf buf) {
		buf.writeInt(this.visualMap.size());
		for (final long serialNumber : this.visualMap.keySet()) {
			buf.writeLong(serialNumber);
			ByteBufUtils.writeUTF8String(buf, this.visualMap.get(serialNumber).get().toString());
		}
	}
}