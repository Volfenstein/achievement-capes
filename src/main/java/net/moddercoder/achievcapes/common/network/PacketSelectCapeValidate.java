package net.moddercoder.achievcapes.common.network;

import java.util.UUID;

import io.netty.buffer.ByteBuf;

import net.minecraft.client.Minecraft;

import net.minecraftforge.fml.relauncher.Side;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.cape.property.CapeProperty;
import net.moddercoder.achievcapes.cape.property.CapePropertyManager;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketSelectCapeValidate implements IMessage, IMessageHandler<PacketSelectCapeValidate, IMessage> {
	
	private UUID playerUUID;
	private boolean validate;
	private long serialNumber;
	
	public PacketSelectCapeValidate() {}
	
	public PacketSelectCapeValidate(UUID playerUUID, long serialNumber, boolean validate) {
		this.validate = validate;
		this.playerUUID = playerUUID;
		this.serialNumber = serialNumber;
	}
	
	@Override
	public IMessage onMessage(PacketSelectCapeValidate message, MessageContext ctx) {
		final boolean validate = message.validate;
		final UUID playerUUID = message.playerUUID;
		final long serialNumber = message.serialNumber;
		
		Minecraft.getMinecraft().addScheduledTask(() -> {
			if (validate) {
				final CapePropertyManager capePropertyManager = AchievementCapes.getCore().getCapePropertyManager(Side.CLIENT);
				final CapeProperty property = AchievementCapes.getCore().getCapePropertyFromPlayer(Side.CLIENT, playerUUID);
				
				AchievementCapes.getCore().setCapePropertyForPlayer(Side.CLIENT, playerUUID,
					property == null ?
						(capePropertyManager.createEmptyProperty())
						:
						(capePropertyManager.ofProperty(property).setCapeSerialNumber(serialNumber)
					)
				);
			}
		});
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.playerUUID = new UUID(buf.readLong(), buf.readLong());
		this.serialNumber = buf.readLong();
		this.validate = buf.readBoolean();
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(this.playerUUID.getMostSignificantBits());
		buf.writeLong(this.playerUUID.getLeastSignificantBits());
		buf.writeLong(this.serialNumber);
		buf.writeBoolean(this.validate);
	}
}