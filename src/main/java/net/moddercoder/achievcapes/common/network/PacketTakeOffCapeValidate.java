package net.moddercoder.achievcapes.common.network;

import java.util.UUID;

import io.netty.buffer.ByteBuf;

import net.minecraft.client.Minecraft;

import net.minecraftforge.fml.relauncher.Side;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.cape.property.CapeProperty;
import net.moddercoder.achievcapes.cape.property.CapePropertyManager;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketTakeOffCapeValidate implements IMessage, IMessageHandler<PacketTakeOffCapeValidate, IMessage> {
	
	private UUID playerUUID;
	private boolean success;
	
	public PacketTakeOffCapeValidate() {}
	
	public PacketTakeOffCapeValidate(UUID playerUUID, boolean success) {
		this.success = success;
		this.playerUUID = playerUUID;
	}
	
	@Override
	public IMessage onMessage(PacketTakeOffCapeValidate message, MessageContext ctx) {
		final boolean state = message.success;
		final UUID playerUUID = message.playerUUID;
		
		Minecraft.getMinecraft().addScheduledTask(() -> {
			final CapePropertyManager capePropertyManager = AchievementCapes.getCore().getCapePropertyManager(Side.CLIENT);
			final CapeProperty property = AchievementCapes.getCore().getCapePropertyFromPlayer(Side.CLIENT, playerUUID);
			if (!property.hasCape() || !state)
				return;
			
			AchievementCapes.getCore().setCapePropertyForPlayer(Side.CLIENT, playerUUID, capePropertyManager.ofProperty(property).setCapeSerialNumberByCape(() -> null));
		});
		
		return null;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.playerUUID = new UUID(buf.readLong(), buf.readLong());
		this.success = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(playerUUID.getMostSignificantBits());
		buf.writeLong(playerUUID.getLeastSignificantBits());
		buf.writeBoolean(success);
	}
}