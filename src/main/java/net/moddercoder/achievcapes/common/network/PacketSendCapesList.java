package net.moddercoder.achievcapes.common.network;

import java.util.Map;
import java.util.HashMap;

import io.netty.buffer.ByteBuf;

import net.minecraft.item.EnumRarity;

import net.minecraft.client.Minecraft;

import net.minecraft.util.ResourceLocation;

import net.minecraftforge.fml.relauncher.Side;

import net.moddercoder.achievcapes.util.RarityUtil;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.cape.ICape;
import net.moddercoder.achievcapes.cape.CapeManager;

import net.minecraftforge.fml.common.network.ByteBufUtils;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketSendCapesList implements IMessage, IMessageHandler<PacketSendCapesList, IMessage> {
	
	private ICape[] capes;
	
	public PacketSendCapesList() {}
	
	public PacketSendCapesList (ICape ... capes) {
		this.capes = capes;
	}
	
	@Override
	public IMessage onMessage(PacketSendCapesList message, MessageContext ctx) {
		final ICape[] capes = message.capes;
		
		Minecraft.getMinecraft().addScheduledTask(() -> {
			final CapeManager clientCapeManager = AchievementCapes.getCore().getCapeManager(Side.CLIENT);
			for (final ICape cape : capes) {
				clientCapeManager.addCape(cape);
			}
		});
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		int capesCount = buf.readInt();
		this.capes = new ICape[capesCount];
		
		for (int i = 0; i < capesCount; i ++) {
			final long serialNumber = buf.readLong();
			final String name = ByteBufUtils.readUTF8String(buf);
			final EnumRarity rarity = RarityUtil.getRarityByName(ByteBufUtils.readUTF8String(buf));
			
			int langCodes = buf.readInt();
			final HashMap<String, String> translates = new HashMap<String, String>();
			for (int j = 0; j < langCodes; j ++) {
				String langCode = ByteBufUtils.readUTF8String(buf);
				String translateName = ByteBufUtils.readUTF8String(buf);
				
				translates.put(langCode, translateName);
			}
			
			this.capes[i] = new ICape() {
				@Override
				public String getName() {
					return name;
				}
				@Override
				public long getSerialNumber() {
					return serialNumber;
				}
				@Override
				public EnumRarity getRarity() {
					return rarity;
				}
				@Override
				public ResourceLocation getAdvancementID() {
					return null;
				}
				@Override
				public Map<String, String> getTranslates() {
					return translates;
				}
			};
		}
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.capes.length);
		for (ICape cape : capes) {
			buf.writeLong(cape.getSerialNumber());
			ByteBufUtils.writeUTF8String(buf, cape.getName());
			ByteBufUtils.writeUTF8String(buf, cape.getRarity().rarityName.toUpperCase());
			
			buf.writeInt(cape.getTranslates().size());
			cape.getTranslates().entrySet().forEach((entry) -> {
				ByteBufUtils.writeUTF8String(buf, entry.getKey());
				ByteBufUtils.writeUTF8String(buf, entry.getValue());
			});
		}
	}
	
}