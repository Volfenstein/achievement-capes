package net.moddercoder.achievcapes.common.network;

import java.util.Map;
import java.util.UUID;
import java.util.List;
import java.util.HashMap;

import io.netty.buffer.ByteBuf;

import net.minecraft.client.Minecraft;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.entity.player.EntityPlayerMP;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.fml.common.FMLCommonHandler;

import net.moddercoder.achievcapes.cape.property.CapeProperty;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketSyncPlayerCapeProperties implements IMessage, IMessageHandler<PacketSyncPlayerCapeProperties, IMessage> {
	
	private Map<UUID, CapeProperty> playersProperty;
	
	public PacketSyncPlayerCapeProperties() {}
	
	@Override
	public IMessage onMessage(PacketSyncPlayerCapeProperties message, MessageContext ctx) {
		final Map<UUID, CapeProperty> playersProperty = message.playersProperty;
		
		Minecraft.getMinecraft().addScheduledTask(() -> {
			playersProperty.entrySet().forEach((entry) -> {
				final UUID uuid = entry.getKey();
				final CapeProperty property = entry.getValue();
				
				AchievementCapes.getCore().setCapePropertyForPlayer(Side.CLIENT, uuid, property);
			});
		});
		
		return null;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.playersProperty = new HashMap<UUID, CapeProperty>();
		
		int playersCount = buf.readInt();
		for (int i = 0; i < playersCount; i ++ ) {
			final UUID playerUUID = new UUID(buf.readLong(), buf.readLong());
			final CapeProperty property = CapeProperty.CapePropertySerializer.read(buf);
			
			this.playersProperty.put(playerUUID, property);
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		this.playersProperty = new HashMap<>();
		
		final List<EntityPlayerMP> players = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers();
		
		players.forEach((player) -> {
			final UUID playerUUID = player.getUniqueID();
			final CapeProperty property = AchievementCapes.getCore().getCapePropertyFromPlayer(Side.SERVER, playerUUID);
			
			this.playersProperty.put(playerUUID, property);
		});
		
		buf.writeInt(this.playersProperty.size());
		this.playersProperty.entrySet().forEach((entry) -> {
			final UUID uuid = entry.getKey();
			buf.writeLong(uuid.getMostSignificantBits());
			buf.writeLong(uuid.getLeastSignificantBits());
			
			final CapeProperty property = entry.getValue();
			property.getSerializer().write(buf);
		});
	}

}
