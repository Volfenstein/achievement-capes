package net.moddercoder.achievcapes.common.cape;

import java.util.Optional;

import net.minecraftforge.fml.common.Loader;

import net.minecraftforge.fml.relauncher.Side;

import net.moddercoder.achievcapes.capes.*;
import net.moddercoder.achievcapes.cape.ICape;
import net.moddercoder.achievcapes.cape.CapesList;
import net.moddercoder.achievcapes.cape.CapeManager;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.common.capedata.loader.CapeLoader;

import net.moddercoder.achievcapes.common.capedata.loader.json.JSONityCape;

public class CommonCapesList {
	
	//=========================DEFAULT-CAPES===========================
	public static final ICape PAN_CAPE = new PanCape();
	public static final ICape STAR_CAPE = new StarCape();
	public static final ICape RUIN_CAPE = new RuinCape();
	public static final ICape GREEN_CAPE = new GreenCape();
	public static final ICape FLAME_CAPE = new FlameCape();
	public static final ICape SPADES_CAPE = new SpadesCape();
	public static final ICape MOJANG_CAPE = new MojangCape();
	public static final ICape VETERAN_CAPE = new VeteranCape();
	public static final ICape SCROLLS_CAPE = new ScrollsCape();
	public static final ICape SNOWMAN_CAPE = new SnowmanCape();
	public static final ICape ENDERMAN_CAPE = new EndermanCape();
	public static final ICape VEGETATION_CAPE = new VegetationCape();
	public static final ICape MARKETPLACE_CAPE = new MarketplaceCape();
	//=================================================================
	
	public static void loadAndRegisterFromWorld() {
		final CapeManager capeManager = AchievementCapes.getCore().getCapeManager(Side.SERVER);
		final JSONityCape[] serverSideJSONityCapes = CapeLoader.loadCapes();
		
		for (final JSONityCape cape : serverSideJSONityCapes) {
			final ICape icape = cape.toICape();
			
			final Optional<ICape> _cape = capeManager.getCapeBySerialNumber(icape.getSerialNumber());
			if (_cape.isPresent()) {
				CommonCapesList.warnAboutTheSameCapes(icape.getName(), _cape.get().getName());
			}
			
			if (cape.dependentendModID != null && !cape.dependentendModID.isEmpty()) {
				if (Loader.isModLoaded(cape.dependentendModID)) {
					capeManager.addCape(icape);
				}
				continue;
			}
			
			capeManager.addCape(icape);
		}
		
		capeManager.addCape(CapesList.DEVELOPER_CAPE);
	}
	
	private static void warnAboutTheSameCapes(String firstName, String secondName) {
		AchievementCapes.logger.warn(String.format("Found capes with the same serial number %s & %s"), firstName, secondName);
	}
}