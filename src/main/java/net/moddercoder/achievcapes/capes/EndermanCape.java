package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.util.ResourceLocations;

public class EndermanCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return ResourceLocations.of("end/dragon_egg");
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Enderman");
		translates.put("ru_ru", "Эндермен");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.UNCOMMON;
	}
	
	@Override
	public long getSerialNumber() {
		return 558497513L;
	}
	
	@Override
	public String getName() {
		return "enderman";
	}
}