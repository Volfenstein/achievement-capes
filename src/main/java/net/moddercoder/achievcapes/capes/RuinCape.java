package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;
import net.moddercoder.achievcapes.util.ResourceLocations;

public class RuinCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return ResourceLocations.of("adventure/sniper_duel");
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Ruin");
		translates.put("ru_ru", "Руины");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.UNCOMMON;
	}
	
	@Override
	public long getSerialNumber() {
		return 333156482L;
	}
	
	@Override
	public String getName() {
		return "ruin";
	}
}