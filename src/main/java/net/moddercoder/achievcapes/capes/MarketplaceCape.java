package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.util.ResourceLocations;

public class MarketplaceCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return ResourceLocations.of("husbandry/bred_all_animals");
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Marketplace");
		translates.put("ru_ru", "Рынок");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.RARE;
	}
	
	@Override
	public long getSerialNumber() {
		return 106739475L;
	}
	
	@Override
	public String getName() {
		return "marketplace";
	}
}