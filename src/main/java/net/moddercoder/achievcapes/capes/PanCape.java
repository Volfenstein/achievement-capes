package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.util.ResourceLocations;

public class PanCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return ResourceLocations.of("husbandry/balanced_diet");
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Pancape");
		translates.put("ru_ru", "Панкейп");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.EPIC;
	}
	
	@Override
	public long getSerialNumber() {
		return 113948990L;
	}
	
	@Override
	public String getName() {
		return "pancape";
	}
}