package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

public class DeveloperCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return null;
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Developer");
		translates.put("ru_ru", "Разработчик");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.EPIC;
	}
	
	@Override
	public long getSerialNumber() {
		return 666666666L;
	}
	
	@Override
	public String getName() {
		return "developer";
	}
}