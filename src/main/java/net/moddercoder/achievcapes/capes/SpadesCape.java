package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.util.ResourceLocations;

public class SpadesCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return ResourceLocations.of("adventure/kill_all_mobs");
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Spades");
		translates.put("ru_ru", "Пики");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.EPIC;
	}
	
	@Override
	public long getSerialNumber() {
		return 866738592L;
	}
	
	@Override
	public String getName() {
		return "spades";
	}
}