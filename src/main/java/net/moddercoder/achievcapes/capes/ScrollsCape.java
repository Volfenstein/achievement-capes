package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.util.ResourceLocations;

public class ScrollsCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return ResourceLocations.of("adventure/totem_of_undying");
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Scrolls");
		translates.put("ru_ru", "\"Свитки\"");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.UNCOMMON;
	}
	
	@Override
	public long getSerialNumber() {
		return 163465564L;
	}
	
	@Override
	public String getName() {
		return "scrolls";
	}
}