package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.util.ResourceLocations;

public class VegetationCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return ResourceLocations.of("story/smelt_iron");
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Vegetation");
		translates.put("ru_ru", "Растительность");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.COMMON;
	}
	
	@Override
	public long getSerialNumber() {
		return 463425314L;
	}
	
	@Override
	public String getName() {
		return "vegetation";
	}
}