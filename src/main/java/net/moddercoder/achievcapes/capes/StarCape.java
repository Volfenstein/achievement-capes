package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.util.ResourceLocations;

public class StarCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return ResourceLocations.of("adventure/adventuring_time");
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Star");
		translates.put("ru_ru", "Звезда");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.EPIC;
	}
	
	@Override
	public long getSerialNumber() {
		return 965154853L;
	}
	
	@Override
	public String getName() {
		return "star";
	}
}