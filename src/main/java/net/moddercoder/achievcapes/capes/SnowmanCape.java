package net.moddercoder.achievcapes.capes;

import java.util.Map;
import java.util.HashMap;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.util.ResourceLocations;

public class SnowmanCape implements ICape {
	@Override
	public ResourceLocation getAdvancementID() {
		return ResourceLocations.of("adventure/summon_iron_golem");
	}
	
	@Override
	public Map<String, String> getTranslates() {
		final HashMap<String, String> translates = new HashMap<String, String>();
		translates.put("en_us", "Snowman");
		translates.put("ru_ru", "Снеговик");
		
		return translates;
	}
	
	@Override
	public EnumRarity getRarity() {
		return EnumRarity.COMMON;
	}
	
	@Override
	public long getSerialNumber() {
		return 884845515L;
	}
	
	@Override
	public String getName() {
		return "snowman";
	}
}