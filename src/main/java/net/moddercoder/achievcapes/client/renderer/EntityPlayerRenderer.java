package net.moddercoder.achievcapes.client.renderer;

import java.util.Optional;

import net.minecraft.init.Items;

import net.minecraft.item.ItemStack;

import net.minecraft.util.math.MathHelper;

import net.moddercoder.achievcapes.cape.ICape;

import net.minecraft.entity.player.EntityPlayer;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import net.minecraft.inventory.EntityEquipmentSlot;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraft.client.renderer.GlStateManager;

import net.moddercoder.achievcapes.client.model.CapeModel;
import net.moddercoder.achievcapes.client.model.CapeModels;

import net.moddercoder.achievcapes.cape.property.CapeProperty;

import net.minecraft.client.renderer.entity.layers.LayerRenderer;

@SideOnly(Side.CLIENT)
public class EntityPlayerRenderer implements LayerRenderer<EntityPlayer> {
	
	private CapeModel model;
	private ItemStack stack;
	private CapeProperty property;
	
	@Override
	public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		if (player == null) return;
		if (player.isInvisible()) return;
		
		this.property = AchievementCapes.getCore().getCapePropertyFromPlayer(Side.CLIENT, player.getUniqueID());
		if (this.property == null) {
			//Sometimes the value can be zero, but this is for one time during the connection of player
			//AchievementCapes.logger.warn("Cape property for some reason is null");
			return;
		}
		if (!this.property.hasCape() | (Boolean)this.property.varByName("hidden").getValue()) {
			return;
		}
		
		this.stack = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
		if (this.stack.isItemEqualIgnoreDurability(Items.ELYTRA.getDefaultInstance())) {
			return;
		}
		
		final Optional<ICape> cape = AchievementCapes.getCore().getCapeManager(Side.CLIENT).getCapeBySerialNumber(property.getCapeSerialNumber());
		if (!cape.isPresent()) {
			AchievementCapes.logger.warn(String.format("Cape with serial number %s on player %s doesn't exists on client! TRY RECONNECT TO SERVER!", property.getCapeSerialNumber(), player.getDisplayName()));
			return;
		}
		final Optional<CapeModel> capeModel = CapeModels.models.get(property.getCapeSerialNumber());
		if (!capeModel.isPresent()) {
			AchievementCapes.logger.warn(property.getCapeSerialNumber() + " hasn't a model. TRY RECONNECT TO SERVER!");
			return;
		}
		
		this.model = capeModel.get();
		
		GlStateManager.pushMatrix();
		
		this.renderRotation(player, partialTicks);
		this.model.render(player, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
		
		GlStateManager.popMatrix();
	}
	
	private void renderRotation (EntityPlayer player, float partialTicks) {
//		final boolean haveChestplate = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST).isEmpty() != true;
		final boolean sneaking = player.isSneaking();
		
		/*
		 *  offsetX ->
		 *  offsetY ->
		 *  offsetZ -> ~0.081
		 *  
		 */
		
		final double desiredX = MathHelper.clampedLerp(player.prevChasingPosX, player.chasingPosX, partialTicks) - MathHelper.clampedLerp(player.prevPosX, player.posX, partialTicks);
		final double desiredY = MathHelper.clampedLerp(player.prevChasingPosY, player.chasingPosY, partialTicks) - MathHelper.clampedLerp(player.prevPosY, player.posY, partialTicks);
		final double desiredZ = MathHelper.clampedLerp(player.prevChasingPosZ, player.chasingPosZ, partialTicks) - MathHelper.clampedLerp(player.prevPosZ, player.posZ, partialTicks);
		
		final float deltaYawOffset = (float)MathHelper.clampedLerp(player.prevRenderYawOffset, player.renderYawOffset, partialTicks);
		final double pointX = (double)MathHelper.sin(deltaYawOffset * 0.017453292F);
		final double pointZ = (double)-MathHelper.cos(deltaYawOffset * 0.017453292F);
		
		final float speed = (float)MathHelper.clampedLerp(player.prevCameraYaw, player.cameraYaw, partialTicks);
		float pitch = MathHelper.clamp((float)desiredY * 10f, -6f, 32f);
		pitch += MathHelper.sin((float)MathHelper.clampedLerp(player.prevDistanceWalkedModified, player.distanceWalkedModified, partialTicks) * 6f) * 32f * speed;
		
		pitch += sneaking ? 25f : 0f;
		
		final float speedPitch = (float)Math.max((desiredX * pointX + desiredZ * pointZ) * 100f, 0f);
//		final float yaw = (float)MathHelper.clamp((desiredX * pointZ - desiredZ * pointX) * 100f, -20f, 20f);
		final float yaw = (float)(desiredX * pointZ - desiredZ * pointX) * 100f;
		
//		GlStateManager.translate(0d, sneaking ? 0.125d : 0f, 0d);
//		GlStateManager.translate(0d, 0d, sneaking ? -0.0625d : 0d);
		
		GlStateManager.translate(0f, 0f, 0.125f);
		
		GlStateManager.rotate(6f + speedPitch / 2f + pitch, 1f, 0f, 0f);
		GlStateManager.rotate(yaw / 2f, 0f, 0f, 1f);
		GlStateManager.rotate(-yaw / 2f, 0f, 1f, 0f);
		GlStateManager.rotate(180f, 0f, 1f, 0f);
		
		GlStateManager.translate(AchievementCapes.offsetX, AchievementCapes.offsetY, AchievementCapes.offsetZ);
		GlStateManager.translate(0f, (sneaking ? 0.125f : 0f), 0f);
		
//		GlStateManager.translate(0f + AchievementCapes.offsetX, sneaking ? 0.4f : 0.15f + AchievementCapes.offsetY, sneaking ? 0.05f : -0.01f + AchievementCapes.offsetZ);
	}

	@Override
	public boolean shouldCombineTextures() {
		return false;
	}
}