package net.moddercoder.achievcapes.client.cache;

import java.util.Map;
import java.util.Arrays;
import java.util.HashMap;

import java.io.File;
import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Files;

import javax.imageio.ImageIO;

import java.util.stream.Collectors;

import java.awt.image.BufferedImage;

import net.minecraft.client.Minecraft;

import java.nio.charset.StandardCharsets;

import net.minecraft.util.ResourceLocation;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.reference.Reference;

import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.TextureManager;

@SideOnly(Side.CLIENT)
public class CapeVisualCache {
	
	private static HashMap<Long, BufferedImage> cacheQueue = new HashMap<>();
	
	private static final String CLIENT_CACHE_DIR_NAME = "." + Reference.MODID + "_client_cache";
	
	private static final File cacheDir = new File(Minecraft.getMinecraft().mcDataDir, CapeVisualCache.CLIENT_CACHE_DIR_NAME);
	
	public static final void cache(final String address) {
		final long serverIndex = CapeVisualCache.setUp(address);
		if (serverIndex == -1L) {
			AchievementCapes.logger.warn("Unable to find a serverIndex!");
			return;
		}
		
		CapeVisualCache.cacheCapes(serverIndex, CapeVisualCache.cacheQueue);
		CapeVisualCache.cacheQueue.clear();
	}
	
	public static void loadCachedVisualMap(final String address) {
		final long serverIndex = CapeVisualCache.setUp(address);
		if (serverIndex == -1L) {
			AchievementCapes.logger.warn("Unable to find a serverIndex!");
			return;
		}
		
		final File serverDir = CapeVisualCache.getServerDir(serverIndex);
		if (serverDir == null) {
			return;
		}
		
		final Minecraft minecraftInstance = Minecraft.getMinecraft();
		
		Minecraft.getMinecraft().addScheduledTask(() -> {
			final File[] serverDirFiles = serverDir.listFiles();
			final TextureManager textureManager = minecraftInstance.getTextureManager();
			for (final File serverDirFile : serverDirFiles) {
				if (serverDirFile.isFile() & serverDirFile.getName().endsWith("png")) {
					try {
						final String capeSerialNumber = serverDirFile.getName().split("\\.")[0];
						
						final DynamicTexture dynamicTexture = new DynamicTexture(ImageIO.read(serverDirFile));
						final ResourceLocation texture = textureManager.getDynamicTextureLocation(capeSerialNumber, dynamicTexture);
						
						AchievementCapes.getCore().getClientVisualMap().put(Long.valueOf(capeSerialNumber), texture);
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}
				}
			}
		});
	}
	
	public static void addToQueue(final Long serialNumber, final BufferedImage capeTexture) {
		if (!CapeVisualCache.cacheQueue.containsKey(serialNumber)) {
			CapeVisualCache.cacheQueue.put(serialNumber, capeTexture);
		}
	}
	
	private static final long setUp(final String address) {
		if (!CapeVisualCache.cacheDir.exists() & !CapeVisualCache.cacheDir.mkdirs()) {
			AchievementCapes.logger.warn("Unable to create client cache directory! \"" + CapeVisualCache.cacheDir.getAbsolutePath() + "\"! That will make work slower.");
		}
		
		long serverIndex = -1L;
		
		final File serverListFile = new File(CapeVisualCache.cacheDir, "servers.json");
		if (serverListFile.exists()) {
			try {
				boolean found = false;
				final String serverReadedData = Files.readAllLines(serverListFile.toPath(), StandardCharsets.UTF_8).stream().collect(Collectors.joining());
				final JsonArray serversJsonArray = new JsonParser().parse(serverReadedData).getAsJsonArray();
				for (int i = 0; i < serversJsonArray.size(); i ++) {
					final String serverAddressFromJson = serversJsonArray.get(i).getAsString();
					if (serverAddressFromJson.equals(address)) {
						serverIndex = i;
						found = true;
						break;
					}
				}
				if (!found) {
					serverIndex = CapeVisualCache.getFoldersCount() > 0 ? CapeVisualCache.getLastServerFolder() + 1 : 0;
					
					serversJsonArray.add(address);
					Files.write(serverListFile.toPath(), AchievementCapes.GSON.toJson(serversJsonArray).getBytes(StandardCharsets.UTF_8));
				}
			} catch (final JsonSyntaxException jse) {
				AchievementCapes.logger.warn("JSON syntax error: " + jse.getMessage() + ", client cache will be resetting!");
				
				serverIndex = 0;
				
				CapeVisualCache.resetCache();
				
				final JsonArray serversJsonArray = new JsonArray();
				serversJsonArray.add(address);
				CapeVisualCache.writeJsonArrayToCache(serverListFile, serversJsonArray);
			} catch (final IOException ioe) {
				ioe.printStackTrace();
			}
		} else {
			serverIndex = 0;
			
			CapeVisualCache.resetCache();
			
			final JsonArray serversJsonArray = new JsonArray();
			serversJsonArray.add(address);
			CapeVisualCache.writeJsonArrayToCache(serverListFile, serversJsonArray);
		}
		
		return serverIndex;
	}
	
	private static final void cacheCapes(long serverIndex, final Map<Long, BufferedImage> capes) {
		final File serverDir = CapeVisualCache.getServerDir(serverIndex);
		if (serverDir == null) {
			return;
		}
		
		capes.forEach((serialNumber, textureImage) -> {
			try {
				final File fileCape = new File(serverDir, String.valueOf(serialNumber) + ".png");
				ImageIO.write(textureImage, "png", fileCape);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		});
	}
	
//	private static final boolean isCapeCached(long serverIndex, long serialNumber) {
//		final File serverDir = CapeVisualCache.getServerDir(serverIndex);
//		if (serverDir == null) {
//			return false;
//		}
//		
//		final File[] serverDirFiles = serverDir.listFiles();
//		for (final File serverDirFile : serverDirFiles) {
//			if (serverDirFile.isFile()) {
//				if (serverDirFile.getName().split("\\.")[0].equals(serialNumber+"")) {
//					return true;
//				}
//			}
//		}
//		
//		return false;
//	}
	
	private static final void writeJsonArrayToCache(final File serverListFile, final JsonArray jsonArray) {
		try {
			Files.write(serverListFile.toPath(), AchievementCapes.GSON.toJson(jsonArray).getBytes(StandardCharsets.UTF_8));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private static final void resetCache() {
		final long foldersCount = CapeVisualCache.getFoldersCount();
		if (foldersCount > 0L) {
			try {
				Files.walk(CapeVisualCache.cacheDir.toPath()).map(Path::toFile).forEach(File::delete);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
	
	private static final File getServerDir(long serverIndex) {
		final File serverDir = new File(CapeVisualCache.cacheDir, ""+serverIndex);
		if (!serverDir.exists() & !serverDir.mkdirs()) {
			AchievementCapes.logger.warn("Unable to create server dir for index: " + serverIndex + "!");
			return null;
		}
		return serverDir;
	}
	
	private static final long getLastServerFolder() {
		return Arrays.stream(CapeVisualCache.cacheDir.listFiles()).collect(Collectors.toList()).stream()
			.filter(File::isDirectory)
			.mapToLong((file) -> Long.valueOf(file.getName()))
			.max()
			.getAsLong();
	}
	
	private static final long getFoldersCount() {
		return Arrays.stream(CapeVisualCache.cacheDir.listFiles()).collect(Collectors.toList()).stream().filter(File::isDirectory).count();
	}
	
}