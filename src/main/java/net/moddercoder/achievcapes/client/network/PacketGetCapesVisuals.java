package net.moddercoder.achievcapes.client.network;

import io.netty.buffer.ByteBuf;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.fml.common.FMLCommonHandler;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

import net.moddercoder.achievcapes.common.network.PacketSendCapesVisuals;

public class PacketGetCapesVisuals implements IMessage, IMessageHandler<PacketGetCapesVisuals, IMessage> {
	
	@Override
	public IMessage onMessage(PacketGetCapesVisuals message, MessageContext ctx) {
		FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
			//Maybe this method doesn't work. Anyway, PacketGetCapesVisuals is not used.
			final PacketSendCapesVisuals packet = new PacketSendCapesVisuals(AchievementCapes.getCore().getVisualMap());
			AchievementCapes.getInstance().network.sendTo(packet, ctx.getServerHandler().player);
		});
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {}
	
	@Override
	public void toBytes(ByteBuf buf) {}
	
}