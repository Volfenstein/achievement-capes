package net.moddercoder.achievcapes.client.network;

import io.netty.buffer.ByteBuf;

import net.moddercoder.achievcapes.cape.ICape;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.entity.player.EntityPlayerMP;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.fml.common.FMLCommonHandler;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

import net.moddercoder.achievcapes.common.network.PacketSendAvailablesCapes;

public class PacketGetAvailablesCapesList implements IMessage, IMessageHandler<PacketGetAvailablesCapesList, IMessage> {
	
	private byte inv;
	
	public PacketGetAvailablesCapesList() {}
	
	public PacketGetAvailablesCapesList(byte inv) {
		this.inv = inv;
	}
	
	@Override
	public IMessage onMessage(PacketGetAvailablesCapesList message, MessageContext ctx) {
		final byte inv = message.inv;
		
		FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
			final EntityPlayerMP requestPlayer = ctx.getServerHandler().player;
			
			final ICape[] availableCapes = AchievementCapes.getCore().getAvailableCapes(Side.SERVER, requestPlayer).toArray(new ICape[0]);
			final long[] serialNumbers = new long[availableCapes.length];
			
			for (int i = 0; i < availableCapes.length; i ++) {
				serialNumbers[i] = availableCapes[i].getSerialNumber();
			}
			
			final PacketSendAvailablesCapes packet = new PacketSendAvailablesCapes(inv, serialNumbers);
			AchievementCapes.getInstance().network.sendTo(packet, ctx.getServerHandler().player);
		});
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.inv = buf.readByte();
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeByte(inv);
	}
}