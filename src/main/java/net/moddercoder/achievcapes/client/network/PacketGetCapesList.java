package net.moddercoder.achievcapes.client.network;

import io.netty.buffer.ByteBuf;

import net.minecraftforge.fml.relauncher.Side;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.fml.common.FMLCommonHandler;

import net.moddercoder.achievcapes.common.network.PacketSendCapesList;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

public class PacketGetCapesList implements IMessage, IMessageHandler<PacketGetCapesList, IMessage> {
	
	@Override
	public IMessage onMessage(PacketGetCapesList message, MessageContext ctx) {
		FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
			final PacketSendCapesList packet = new PacketSendCapesList(AchievementCapes.getCore().getCapeManager(Side.SERVER).getAllCapes().toArray(new ICape[0]));
			AchievementCapes.getInstance().network.sendTo(packet, ctx.getServerHandler().player);
		});
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {}
	
	@Override
	public void toBytes(ByteBuf buf) {}
}