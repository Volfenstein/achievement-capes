package net.moddercoder.achievcapes.client.network;

import java.util.Collection;

import io.netty.buffer.ByteBuf;

import net.minecraftforge.fml.relauncher.Side;

import net.moddercoder.achievcapes.cape.ICape;

import net.minecraft.entity.player.EntityPlayerMP;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.fml.common.FMLCommonHandler;

import net.moddercoder.achievcapes.cape.property.CapeProperty;
import net.moddercoder.achievcapes.cape.property.CapePropertyManager;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

import net.moddercoder.achievcapes.common.network.PacketSelectCapeValidate;

public class PacketSelectCape implements IMessage, IMessageHandler<PacketSelectCape, IMessage> {
	
	private long serialNumber;
	
	public PacketSelectCape() {}
	
	public PacketSelectCape (long serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	@Override
	public IMessage onMessage(PacketSelectCape message, MessageContext ctx) {
		final long serialNumber = message.serialNumber;
		
		final EntityPlayerMP player = ctx.getServerHandler().player;
		
		FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
			boolean can = false;
			long selectedSerialNumber = -1L;
			
			final Collection<ICape> capes = AchievementCapes.getCore().getAvailableCapes(Side.SERVER, player.getUniqueID());
			
			for (final ICape cape : capes) {
				if (cape.getSerialNumber() == serialNumber) {
					selectedSerialNumber = serialNumber;
					can = true;
					break;
				}
			}
			
			final PacketSelectCapeValidate packet = new PacketSelectCapeValidate(player.getUniqueID(), selectedSerialNumber, can);
			AchievementCapes.getInstance().network.sendToAll(packet);
			
			if (can) {
				final CapePropertyManager capePropertyManager = AchievementCapes.getCore().getCapePropertyManager(Side.SERVER);
				
				final CapeProperty property = AchievementCapes.getCore().getCapePropertyFromPlayer(Side.SERVER, player.getUniqueID());
				AchievementCapes.getCore().setCapePropertyForPlayer(Side.SERVER, player.getUniqueID(), capePropertyManager.ofProperty(property).setCapeSerialNumber(selectedSerialNumber));
			}
		});
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.serialNumber = buf.readLong();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(serialNumber);
	}
	
}