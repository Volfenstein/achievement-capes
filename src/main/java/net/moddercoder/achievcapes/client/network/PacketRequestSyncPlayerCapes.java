package net.moddercoder.achievcapes.client.network;

import io.netty.buffer.ByteBuf;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.fml.common.FMLCommonHandler;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

import net.moddercoder.achievcapes.common.network.PacketSyncPlayerCapeProperties;

public class PacketRequestSyncPlayerCapes implements IMessage, IMessageHandler<PacketRequestSyncPlayerCapes, IMessage>{
	
	@Override
	public IMessage onMessage(PacketRequestSyncPlayerCapes message, MessageContext ctx) {
		FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
			final PacketSyncPlayerCapeProperties packetSyncPlayers = new PacketSyncPlayerCapeProperties();
			AchievementCapes.getInstance().network.sendTo(packetSyncPlayers, ctx.getServerHandler().player);
		});
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {}
	
	@Override
	public void toBytes(ByteBuf buf) {}
	
}