package net.moddercoder.achievcapes.client.network;

import java.util.UUID;

import io.netty.buffer.ByteBuf;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.entity.player.EntityPlayerMP;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.fml.common.FMLCommonHandler;

import net.moddercoder.achievcapes.cape.property.CapeProperty;
import net.moddercoder.achievcapes.cape.property.CapePropertyManager;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;

import net.moddercoder.achievcapes.common.network.PacketTakeOffCapeValidate;

public class PacketTakeOffCape implements IMessage, IMessageHandler<PacketTakeOffCape, IMessage> {
	
	@Override
	public IMessage onMessage(PacketTakeOffCape message, MessageContext ctx) {
		final EntityPlayerMP player = ctx.getServerHandler().player;
		
		FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> {
			final CapeProperty property = AchievementCapes.getCore().getCapePropertyFromPlayer(Side.SERVER, player::getUniqueID);
			final CapePropertyManager capePropertyManager = AchievementCapes.getCore().getCapePropertyManager(Side.SERVER);
			
			final UUID playerUUID = player.getUniqueID();
			
			final PacketTakeOffCapeValidate packet = new PacketTakeOffCapeValidate(playerUUID, true);
			AchievementCapes.getInstance().network.sendToAll(packet);
			
			AchievementCapes.getCore().setCapePropertyForPlayer(Side.SERVER, playerUUID, capePropertyManager.ofProperty(property).setCapeSerialNumberByCape(() -> null));
		});
		
		return null;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {}
	
	@Override
	public void toBytes(ByteBuf buf) {}
	
}