package net.moddercoder.achievcapes.client.model;

import java.util.Optional;
import java.util.LinkedHashMap;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class CapeModels {
	
	public static LinkedHashMap<Long, Optional<CapeModel>> models = new LinkedHashMap<Long, Optional<CapeModel>>();
	
	public static void reset () {
		CapeModels.models = new LinkedHashMap<Long, Optional<CapeModel>>();
	}
	
}