package net.moddercoder.achievcapes.client.model;

import net.minecraft.entity.Entity;

import net.minecraft.client.Minecraft;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import net.minecraft.client.renderer.GlStateManager;

import net.moddercoder.achievcapes.AchievementCapes;

@SideOnly(Side.CLIENT)
public class CapeModel extends ModelBiped {
	
	public final ModelRenderer renderer;
	
	private final ResourceLocation TEXTURE;
	
	public CapeModel (final ICape CAPE) throws NullPointerException {
		this.textureWidth = 22;
		this.textureHeight = 17;
		this.TEXTURE = AchievementCapes.getCore().getClientVisualMap().get(CAPE.getSerialNumber());
		
		this.renderer = new ModelRenderer(this, 0, 0);
		
		this.renderer.setRotationPoint(0f, 0f, 0f);
		this.renderer.addBox(-5f, 0f, -1f, 10, 16, 1);
		
		this.bipedBody.addChild(this.renderer);
	}
	
	@Override
	public void render(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		if (TEXTURE != null) {
			GlStateManager.color(1f, 1f, 1f, 1f);
			Minecraft.getMinecraft().renderEngine.bindTexture(TEXTURE);
			renderer.render(0.0625f);
		}
	}
	
}