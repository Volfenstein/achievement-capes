package net.moddercoder.achievcapes.client.gui;

import net.minecraft.client.gui.GuiScreen;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.cape.ICape;

import net.moddercoder.achievcapes.reference.Reference;

public abstract class AbstractGuiCapeChooserScreen extends GuiScreen {
	
	public static final ResourceLocation WIDGETS_TEXTURE = new ResourceLocation(Reference.MODID, "textures/gui/widgets.png");
	
	protected ICape[] availableCapes;
	
	public AbstractGuiCapeChooserScreen(ICape ... availableCapes) {
		this.availableCapes = availableCapes;
		this.allowUserInput = true;
	}
	
	public ICape[] getAvailableCapes() {
		return this.availableCapes;
	}
	
}