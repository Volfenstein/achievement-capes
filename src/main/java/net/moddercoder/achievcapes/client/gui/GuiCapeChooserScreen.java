package net.moddercoder.achievcapes.client.gui;

import java.util.Optional;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import java.net.URI;
import java.net.URISyntaxException;

import net.minecraft.client.Minecraft;

import net.minecraft.client.gui.GuiButton;

import net.moddercoder.achievcapes.cape.ICape;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import net.minecraft.client.entity.EntityPlayerSP;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.GlStateManager;

import net.moddercoder.achievcapes.cape.config.Configs;

import net.moddercoder.achievcapes.reference.Reference;

import net.minecraft.client.renderer.entity.RenderManager;

import net.moddercoder.achievcapes.client.model.CapeModel;
import net.moddercoder.achievcapes.client.model.CapeModels;

import net.moddercoder.achievcapes.util.RarityUtil;
import net.moddercoder.achievcapes.util.LocalizationString;

import net.moddercoder.achievcapes.cape.property.CapeProperty;

import net.moddercoder.achievcapes.client.network.PacketSelectCape;
import net.moddercoder.achievcapes.client.network.PacketTakeOffCape;
import net.moddercoder.achievcapes.client.network.PacketRequestSyncPlayerCapes;

import net.moddercoder.achievcapes.client.gui.button.AchievementCapesButton;
import net.moddercoder.achievcapes.client.gui.button.AchievementCapesImageButton;

@SideOnly(Side.CLIENT)
public class GuiCapeChooserScreen extends AbstractGuiCapeChooserScreen {
	
	private static URI IMGUR_URI;
	static {
		try {
			GuiCapeChooserScreen.IMGUR_URI = new URI("https://imgur.com/");
		} catch (final URISyntaxException e) {
			AchievementCapes.logger.error("Can't open url for {}", e);
		}
	}
	
	private int capeX;
	private int capeY;
	private int defaultCapeX;
	private int defaultCapeY;
	
	private Optional<ICape> selectedCape = Optional.<ICape>empty();
	
	private int page = 0;
	public String langCode;
	
	private float ticks = 0;
	
	private float oldMouseX = 0;
	private float oldMouseY = 0;
	
	//Animation
	private final int ANIMATION_DURATION = 1 * 5;
	
	private int moveCapeX = 0;
	private int incrementPage = 0;
	private int startAnimationRenders = 0;
	private boolean animationPlaying = false;
	//...
	
	private AchievementCapesButton nextPageBtn;
	private AchievementCapesButton prevPageBtn;
	private AchievementCapesButton selectButton;
	private AchievementCapesButton takeOffButton;
	private AchievementCapesImageButton syncButton;
	private AchievementCapesImageButton closeButton;
	private AchievementCapesImageButton imgurButton;
	
	public GuiCapeChooserScreen (ICape ... capes) {
		super(capes);
	}
	
	@Override
	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		
		this.refreshGUI();
		this.updateButtonEnabled();
		
		this.langCode = super.mc.getLanguageManager().getCurrentLanguage().getLanguageCode();
		
		super.initGui();
	}
	
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}
	
	private void refreshGUI () {
		this.buttonList.clear();
		this.refreshMainMenu();
	}
	
	private void refreshMainMenu () {
		final Optional<ICape> cape = AchievementCapes.getCore().getCapeManager(Side.CLIENT).getCapeBySerialNumber(AchievementCapes.getCore().getCapePropertyFromPlayer(Side.CLIENT, super.mc.player::getUniqueID).getCapeSerialNumber());
		cape.ifPresent((_cape) -> this.selectedCape = Optional.of(_cape));
		
		if (super.availableCapes.length > 0) {
			if (this.selectedCape.isPresent()) {
				for (int i = 0; i < super.availableCapes.length; i ++) {
					if (this.selectedCape.get().getSerialNumber() == super.availableCapes[i].getSerialNumber()) {
						this.page = i;
						break;
					}
				}
			} else {
				this.selectedCape = Optional.ofNullable(super.availableCapes[0]);
			}
		}
		
		this.capeX = this.width / 2;
		this.capeY = this.height / 2 - 50;
		this.defaultCapeX = capeX;
		this.defaultCapeY = capeY;
		
		this.closeButton = new AchievementCapesImageButton(0, this.width - 20, 4, 16, 0, 16, 16, AbstractGuiCapeChooserScreen.WIDGETS_TEXTURE);
		this.addButton(closeButton);
		
		this.selectButton = new AchievementCapesButton(1, this.width / 2 + 2, this.height - 24, 73, 18, LocalizationString.ofText("select"));
		this.addButton(selectButton);
		
		this.takeOffButton = new AchievementCapesButton(2, this.width / 2 - 73, this.height - 24, 73, 18, LocalizationString.ofText("take_off"));
		this.addButton(takeOffButton);
		
		this.syncButton = new AchievementCapesImageButton(3, this.width - 40, 4, 0, 0, 16, 16, AbstractGuiCapeChooserScreen.WIDGETS_TEXTURE);
		this.addButton(syncButton);
		
		this.imgurButton = new AchievementCapesImageButton(4, 4, 4, 32, 0, 34, 16, AbstractGuiCapeChooserScreen.WIDGETS_TEXTURE);
		this.addButton(imgurButton);
		
		if (this.availableCapes.length > 1) {
			this.prevPageBtn = new AchievementCapesButton(5, this.takeOffButton.x, this.height / 2 + 64, 16, 16, "<");
			this.nextPageBtn = new AchievementCapesButton(6, this.selectButton.x + this.selectButton.width - 16, this.height / 2 + 64, 16, 16, ">");
			
			this.addButton(prevPageBtn);
			this.addButton(nextPageBtn);
		}
	}
	
	private void updateButtonEnabled () {
		final CapeProperty playerCapeProperty = AchievementCapes.getCore().getCapePropertyFromPlayer(Side.CLIENT, super.mc.player::getUniqueID);
		
		final long wearingCapeSerialNumber = playerCapeProperty.getCapeSerialNumber();
		
		this.takeOffButton.enabled = wearingCapeSerialNumber != -1L;
		
		if (this.availableCapes.length > 0) {
			if (playerCapeProperty.hasCape()) {
				this.selectButton.enabled = wearingCapeSerialNumber == this.availableCapes[this.page].getSerialNumber() ? false : true;
			}
		} else {
			this.selectButton.enabled = false;
		}
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawDefaultBackground();
		
		super.drawScreen(mouseX, mouseY, partialTicks);
		
		final String versionStr = "v".concat(String.valueOf(Reference.VERSION));
		final String authorStr = "Made by ".concat(String.valueOf(Reference.AUTHOR));
		this.drawString(this.fontRenderer, authorStr, 3, this.height - this.fontRenderer.FONT_HEIGHT - 1, 0xaaaaaa);
		this.drawString(this.fontRenderer, versionStr, this.width - this.fontRenderer.getStringWidth(versionStr) - 4, this.height - this.fontRenderer.FONT_HEIGHT - 1, 0xaaaaaa);
		this.drawCenteredString(this.fontRenderer, LocalizationString.ofText("enterValue", Reference.NAME), this.width / 2, this.height / 2 - 90, 0xffffff);
		
		if (availableCapes.length > 0) {
			final String pageString = String.valueOf((page + 1)) + "/" + String.valueOf(availableCapes.length);
			this.drawCenteredString(this.fontRenderer, pageString, (this.width / 2), (this.height / 2 + 68), 0xffffff);
			
			final Optional<CapeModel> capeModel = CapeModels.models.get(availableCapes[page].getSerialNumber());
			this.drawCapeOnScreen(capeModel, this.capeX, this.capeY, 80, 90, partialTicks);
			
			final String capeName = this.availableCapes[this.page].getTranslates().containsKey(langCode) ? this.availableCapes[this.page].getTranslates().get(langCode) : this.availableCapes[this.page].getTranslates().get("en_us");
			this.drawCenteredString(this.fontRenderer, capeName, (this.width / 2), (this.height / 2 + this.height / 3) + 2, RarityUtil.getColorByRarity(this.availableCapes[this.page].getRarity()));
			GlStateManager.color(1f, 1f, 1f);
		} else {
			final String str = LocalizationString.ofText("haventCapes");
			this.drawCenteredString(this.fontRenderer, str, (this.width / 2), (this.height / 2), 0xffffff);
		}
		
		this.drawPlayerOnScreen(this.width / 2 - 120f, this.height / 2 + 60f);
		
		this.animationUpdate(partialTicks);
		
		this.oldMouseX = (float)mouseX;
		this.oldMouseY = (float)mouseY;
		
		this.ticks += partialTicks;
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button == this.closeButton) {
			super.mc.player.closeScreen();
		} else if (button == this.selectButton) {
			if (!this.selectedCape.isPresent()) return;
			final PacketSelectCape packet = new PacketSelectCape(this.selectedCape.get().getSerialNumber());
			AchievementCapes.getInstance().network.sendToServer(packet);
			
			this.takeOffButton.enabled = true;
			this.selectButton.enabled = false;
		} else if (button == this.syncButton) {
			final PacketRequestSyncPlayerCapes packet = new PacketRequestSyncPlayerCapes();
			AchievementCapes.getInstance().network.sendToServer(packet);
		} else if (button == this.takeOffButton){
			final PacketTakeOffCape packet = new PacketTakeOffCape();
			AchievementCapes.getInstance().network.sendToServer(packet);
			this.takeOffCape();
			
			this.takeOffButton.enabled = false;
			this.selectButton.enabled = true;
		} else if (button == this.nextPageBtn) {
			if (Configs.guiAnimation) {
				if (!this.animationPlaying) {
					this.incrementPage = 1;
					this.moveCapeX = this.defaultCapeX - 150;
					this.slide();
				}
			} else {
				this.nextCape();
			}
		} else if (button == this.prevPageBtn) {
			if (Configs.guiAnimation) {
				if (!this.animationPlaying) {
					this.incrementPage = -1;
					this.moveCapeX = this.defaultCapeX + 210;
					this.slide();
				}
			} else {
				this.prevCape();
			}
		} else if (button == this.imgurButton) {
			if (this.mc.gameSettings.chatLinks) {
				this.openWebLink(GuiCapeChooserScreen.IMGUR_URI);
			}
		}
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		super.keyTyped(typedChar, keyCode);
		
		if (this.availableCapes.length > 0) {
			if (this.mc.gameSettings.keyBindJump.getKeyCode() == keyCode) {
				if(!this.selectedCape.isPresent()) return;
				//Cursed logic
				if (this.takeOffButton.enabled & !this.selectButton.enabled) {
					final PacketTakeOffCape packet = new PacketTakeOffCape();
					AchievementCapes.getInstance().network.sendToServer(packet);
					this.takeOffCape();
					
					this.takeOffButton.enabled = false;
					this.selectButton.enabled = true;
				} else {
					final PacketSelectCape packet = new PacketSelectCape(this.selectedCape.get().getSerialNumber());
					AchievementCapes.getInstance().network.sendToServer(packet);
					
					this.takeOffButton.enabled = true;
					this.selectButton.enabled = false;
				}
			}
			if (this.availableCapes.length > 1) {
				if (this.mc.gameSettings.keyBindRight.getKeyCode() == keyCode | Keyboard.KEY_RIGHT == keyCode) {
					if (Configs.guiAnimation) {
						if (!this.animationPlaying) {
							this.incrementPage = 1;
							this.moveCapeX = this.defaultCapeX - 150;
							this.slide();
						}
					} else {
						this.nextCape();
					}
				}
				if (this.mc.gameSettings.keyBindLeft.getKeyCode() == keyCode | Keyboard.KEY_LEFT == keyCode) {
					if (Configs.guiAnimation) {
						if (!this.animationPlaying) {
							this.incrementPage = -1;
							this.moveCapeX = this.defaultCapeX + 210;
							this.slide();
						}
					} else {
						this.prevCape();
					}
				}
			}
		}
	}
	
	private void drawCapeOnScreen (final Optional<CapeModel> MODEL, double x, double y, float width, float height, float partialTicks) {
		if (MODEL.isPresent() == false) {
			return;
		}
		
		GlStateManager.enableColorMaterial();
		GlStateManager.disableLighting();
		GlStateManager.enableBlend();
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, 40d);
		GlStateManager.scale(width, height, 0f);
		GlStateManager.rotate(Configs.guiCapeRotationSpeed * this.ticks, 0f, 1f, 0f);
		GlStateManager.rotate(-6f, 1f, 0f, 0f);
		GlStateManager.translate(0d, 0d, 0.055d);
		
		MODEL.get().render(null, 0f, 0f, 0f, 0f, 0f, 0f);
		
		GlStateManager.popMatrix();
		GlStateManager.disableRescaleNormal();
		GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GlStateManager.disableTexture2D();
		GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}
	
	private void drawPlayerOnScreen (double x, double y) {
		GlStateManager.color(1f, 1f, 1f, 1f);
		GlStateManager.enableColorMaterial();
		GlStateManager.disableLighting();
		GlStateManager.enableBlend();
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, 100d);
		GlStateManager.scale(-60f, 63f, 60f); 
		GlStateManager.rotate(180f, 0f, 0f, 1f);
		GlStateManager.rotate(((float)(Math.atan((double)this.oldMouseY / 40d)) * 5f), 1f, 0f, 0f);
		final RenderManager renderManager = Minecraft.getMinecraft().getRenderManager();
		
		final EntityPlayerSP player = super.mc.player;
		float rYaw = player.rotationYaw;
		float rPitch = player.rotationPitch;
		float rYawHead = player.rotationYawHead;
		float rYawOffset = player.renderYawOffset;
		float rPrevYawHead = player.prevRotationYaw;
		final CapeProperty prevCapeProperty = AchievementCapes.getCore().getCapePropertyFromPlayer(Side.CLIENT, super.mc.player::getUniqueID);
		
		final double centerMouseX = this.width / 2 - oldMouseX;
		final double centerMouseY = this.height / 2 - oldMouseY;
		
		player.rotationYaw = 180f - (float)Math.atan((double)centerMouseX / 40d) * 40f;
		player.rotationPitch = -(float)Math.atan((double)centerMouseY / 80d) * 10f - 4f;
		player.renderYawOffset = 180f - (float)Math.atan((double)centerMouseX / 40d) * 20f;
		player.rotationYawHead = player.rotationYaw;
		player.prevRotationYaw = player.rotationYaw;
		AchievementCapes.getCore().setCapePropertyForPlayer(Side.CLIENT, player.getUniqueID(), AchievementCapes.getCore().getCapePropertyManager(Side.CLIENT).createProperty(() -> 
			this.selectedCape.isPresent() ? this.selectedCape.get() : null
		));
		
		renderManager.setPlayerViewY(180f);
		renderManager.setRenderShadow(false);
		renderManager.renderEntity(player, 0d, 0d, 0d, 0f, 1f, true);
		renderManager.setRenderShadow(true);
		
		player.rotationYaw = rYaw;
		player.rotationPitch = rPitch;
		player.rotationYawHead = rYawHead;
		player.renderYawOffset = rYawOffset;
		player.prevRotationYaw = rPrevYawHead;
		AchievementCapes.getCore().setCapePropertyForPlayer(Side.CLIENT, player.getUniqueID(), AchievementCapes.getCore().getCapePropertyManager(Side.CLIENT).ofProperty(prevCapeProperty));
		
		GlStateManager.popMatrix();
		GlStateManager.disableRescaleNormal();
		GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GlStateManager.disableTexture2D();
		GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}
	
	private void takeOffCape () {
		if (this.availableCapes.length > 0) {
			for (int i = 0; i < availableCapes.length; i ++) {
				this.page = 0;
				if (this.selectedCape.get().getSerialNumber() == availableCapes[i].getSerialNumber()) {
					this.page = i;
					break;
				}
			}
			return;
		}
		
		this.selectedCape = Optional.empty();
	}
	
	private void animationUpdate (float partialTicks) {
		if (this.animationPlaying) {
			this.capeX += (float)(this.moveCapeX - capeX) * 0.05f * partialTicks;
			if (this.ticks - startAnimationRenders > ANIMATION_DURATION) {
				this.animationEnd();
			}
		}
	}
	
	private void openWebLink(URI url) {
        try {
            Class<?> oclass = Class.forName("java.awt.Desktop");
            Object object = oclass.getMethod("getDesktop").invoke((Object)null);
            oclass.getMethod("browse", URI.class).invoke(object, url);
        } catch (Throwable _throwable) {
            final Throwable throwable = _throwable.getCause();
            AchievementCapes.logger.error("Couldn't open link: {}", (Object)(throwable == null ? "<UNKNOWN>" : throwable.getMessage()));
        }
    }
	
	private void animationEnd () {
		this.setCape(this.page + this.incrementPage);
		
		this.animationPlaying = false;
		this.capeX = defaultCapeX;
		this.capeY = defaultCapeY;
		
		this.incrementPage = 0;
		this.moveCapeX = 0;
	}
	
	private void slide() {
		if (!this.animationPlaying) {
			this.animationPlaying = true;
			this.startAnimationRenders = (int)this.ticks;
		}
	}
	
	private void setCape(int page) {
		if (page < 0) {
			this.page = this.availableCapes.length - 1;
		} else if (page >= this.availableCapes.length) {
			this.page = 0;
		} else {
			this.page = page;
		}
		
		this.selectedCape = Optional.of(this.availableCapes[this.page]);
		this.updateButtonEnabled();
	}
	
	private void nextCape() {
		this.setCape(this.page + 1);
	}
	
	private void prevCape() {
		this.setCape(this.page - 1);
	}
	
	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
	
}