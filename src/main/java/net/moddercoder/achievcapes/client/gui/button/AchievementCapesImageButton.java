package net.moddercoder.achievcapes.client.gui.button;

import net.minecraft.client.Minecraft;

import net.minecraft.util.ResourceLocation;

import net.minecraft.client.renderer.GlStateManager;

public class AchievementCapesImageButton extends AchievementCapesButton {
	
	protected int textureX;
	protected int textureY;
	
	protected ResourceLocation texture;
	
	public AchievementCapesImageButton(int id, int x, int y, int textureX, int textureY, int width, int height, ResourceLocation texture) {
		super(id, x, y, width, height, "");
		this.texture = texture;
		this.textureX = textureX;
		this.textureY = textureY;
	}
	
	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
		if (this.visible) {
			super.drawButton(mc, mouseX, mouseY, partialTicks);
			mc.getTextureManager().bindTexture(this.texture);
			this.hovered = mouseX >= this.x && mouseX < this.x + this.width && mouseY >= this.y && mouseY < this.y + this.height;
			final int state = getState(this.clicked);
			GlStateManager.enableBlend();
			GlStateManager.color(1f, 1f, 1f, 1f);
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			this.drawTexturedModalRect(this.x, this.y, this.textureX, this.textureY + state * this.height, this.width, this.height);
			
			this.mouseDragged(mc, mouseX, mouseY);
		}
	}
	
}
