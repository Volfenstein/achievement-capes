package net.moddercoder.achievcapes.client.gui.button;

import net.minecraft.client.Minecraft;

import net.minecraft.util.ResourceLocation;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.FontRenderer;

import net.minecraft.client.renderer.GlStateManager;

import net.moddercoder.achievcapes.reference.Reference;

public class AchievementCapesButton extends GuiButton {
	
	private static final ResourceLocation TEXTURE = new ResourceLocation(Reference.MODID, "textures/gui/buttons.png");
	
	public static final int MIN_WIDTH = 16;
	public static final int MIN_HEIGHT = 16;
	public static final int MAX_WIDTH = 128;
	public static final int CORNER_WIDTH = 3;
	public static final int MAX_HEIGHT = MAX_WIDTH;
	public static final int CORNER_HEIGHT = CORNER_WIDTH;
	
	public static final int TOP_EDGE_HEIGHT = 3;
	public static final int LEFT_EDGE_WIDTH = 3;
	public static final int RIGHT_EDGE_WIDTH = 3;
	public static final int BOTTOM_EDGE_HEIGHT = 3;
	
	public static final int LEFT_COORD = CORNER_WIDTH;
	public static final int TOP_COORD = CORNER_HEIGHT;
	public static final int DOUBLE_CORNER_WIDTH = CORNER_WIDTH * 2;
	public static final int RIGHT_COORD = MAX_WIDTH - CORNER_WIDTH;
	public static final int DOUBLE_CORNER_HEIGHT = CORNER_HEIGHT * 2;
	public static final int BOTTOM_COORD = MAX_HEIGHT - CORNER_HEIGHT;
	
	protected boolean clicked = false;
	
	public int textColor = 0;
	public boolean shadowText = false;
	
	public AchievementCapesButton (int id, int x, int y, int width, int height, String text) {
		super(id, x, y, Math.max(MIN_WIDTH, Math.min(MAX_WIDTH, width)), Math.max(MIN_HEIGHT, Math.min(MAX_HEIGHT, height)), text);
	}
	
	protected int getState (boolean clicked) {
		int state = this.getHoverState(this.hovered);
		if (state != 0)
			state = this.clicked ? 3 : state;
		return state;
	}
	
	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
		if (this.visible) {
			super.drawButton(mc, mouseX, mouseY, partialTicks);
			mc.getTextureManager().bindTexture(AchievementCapesButton.TEXTURE);
			this.hovered = mouseX >= this.x && mouseX < this.x + this.width && mouseY >= this.y && mouseY < this.y + this.height;
			final int state = getState(this.clicked);
			GlStateManager.enableBlend();
			GlStateManager.color(1f, 1f, 1f, 1f);
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			
			int textureX = state / 2;
			int textureY = state % 2;
			this.drawTexturedModalRect(this.x, this.y, textureX * MAX_WIDTH, textureY * MAX_HEIGHT, this.width, TOP_EDGE_HEIGHT);
			this.drawTexturedModalRect(this.x, this.y + CORNER_HEIGHT, textureX * MAX_WIDTH, (textureY * MAX_HEIGHT) + CORNER_HEIGHT, LEFT_EDGE_WIDTH, this.height - TOP_EDGE_HEIGHT);
			this.drawTexturedModalRect(this.x + CORNER_WIDTH, this.y + CORNER_HEIGHT, (textureX * MAX_WIDTH) + LEFT_COORD, (textureY * MAX_HEIGHT) + TOP_COORD, this.width - DOUBLE_CORNER_WIDTH, this.height - DOUBLE_CORNER_HEIGHT);
			this.drawTexturedModalRect(this.x + this.width - CORNER_WIDTH, this.y, (textureX * MAX_WIDTH) + RIGHT_COORD, textureY * MAX_HEIGHT, RIGHT_EDGE_WIDTH, this.height - BOTTOM_EDGE_HEIGHT);
			this.drawTexturedModalRect(this.x, this.y + this.height - CORNER_HEIGHT, textureX * MAX_WIDTH, (textureY * MAX_HEIGHT) + BOTTOM_COORD, this.width, BOTTOM_EDGE_HEIGHT);
			this.drawTexturedModalRect(this.x + this.width - CORNER_WIDTH, this.y + this.height - CORNER_HEIGHT, (textureX * MAX_WIDTH) + RIGHT_COORD, (textureY * MAX_HEIGHT) + BOTTOM_COORD, CORNER_WIDTH, CORNER_HEIGHT);
			
			this.mouseDragged(mc, mouseX, mouseY);
			
			this.drawButtonForegroundLayer(mouseX, mouseY);
		}
	}
	
	@Override
	public void drawButtonForegroundLayer(int mouseX, int mouseY) {
		super.drawButtonForegroundLayer(mouseX, mouseY);
		
		FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
		
		int color = 0x646464;
		
		if (this.textColor == 0) {
			if (this.enabled) {
				if (this.clicked) {
					color = 0x99FF66;
				} else {
					if (this.hovered) {
						color = 0xE0E0E0;
					}
				}
			} else {
				color = 0x595959;
			}
		} else {
			color = this.textColor;
		}
		
		fontRenderer.drawString(displayString, this.x + this.width / 2 - fontRenderer.getStringWidth(displayString) / 2, this.y + (this.height - 8) / 2, color, this.shadowText);
	}
	
	@Override
	public void mouseReleased(int mouseX, int mouseY) {
		this.clicked = false;
	}
	
	@Override
	public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
		boolean state = super.mousePressed(mc, mouseX, mouseY);
		this.clicked = state;
		return state;
	}
	
}