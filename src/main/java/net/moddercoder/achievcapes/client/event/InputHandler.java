package net.moddercoder.achievcapes.client.event;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.Minecraft;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import net.moddercoder.achievcapes.AchievementCapes;

import net.minecraftforge.client.event.GuiScreenEvent;

import net.minecraftforge.fml.common.gameevent.InputEvent;

import net.moddercoder.achievcapes.client.proxy.ClientProxy;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import net.moddercoder.achievcapes.client.gui.AbstractGuiCapeChooserScreen;

import net.moddercoder.achievcapes.client.network.PacketGetAvailablesCapesList;

@SideOnly(Side.CLIENT)
public class InputHandler {
	@SubscribeEvent
	public void keyInput (InputEvent.KeyInputEvent event) {
		final Minecraft minecraft = Minecraft.getMinecraft();
		if (AchievementCapes.hasInternetConnection) {
			if (minecraft.player != null && minecraft.currentScreen == null) {
				if (ClientProxy.KEY_OPEN_CAPE_MENU1.isPressed()) {
					final byte guiIndex = Keyboard.isKeyDown(Keyboard.KEY_RSHIFT) ? (byte)1 : (byte)0;
					AchievementCapes.getInstance().network.sendToServer(new PacketGetAvailablesCapesList(guiIndex));
				}
			}
		}
	}
	
	@SubscribeEvent
	public void guiKeyPressed (GuiScreenEvent.KeyboardInputEvent event) {
		if (AchievementCapes.hasInternetConnection) {
			if (event.getGui() instanceof AbstractGuiCapeChooserScreen) {
				if (Keyboard.getEventKeyState()) {
					if (Keyboard.getEventKey() == ClientProxy.KEY_OPEN_CAPE_MENU1.getKeyCode() | Keyboard.getEventKey() == Minecraft.getMinecraft().gameSettings.keyBindInventory.getKeyCode()) {
						event.getGui().mc.player.closeScreen();
					}
				}
			}
		}
	}
}
