package net.moddercoder.achievcapes.client.event;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import net.moddercoder.achievcapes.AchievementCapes;

import net.moddercoder.achievcapes.client.model.CapeModels;

import net.minecraftforge.fml.common.network.FMLNetworkEvent;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@SideOnly(Side.CLIENT)
public class ClientPlayerConnectionEvent {
	@SubscribeEvent
	public void onDisconnect (FMLNetworkEvent.ClientDisconnectionFromServerEvent event) {
		AchievementCapes.getCore().resetAll(Side.CLIENT);
		CapeModels.reset();
	}
}