package net.moddercoder.achievcapes.client.proxy;

import java.util.Map;

import net.minecraft.item.Item;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.Minecraft;

import net.minecraftforge.common.MinecraftForge;

import net.minecraft.client.settings.KeyBinding;

import net.moddercoder.achievcapes.proxy.IProxy;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import net.minecraftforge.client.model.ModelLoader;

import net.moddercoder.achievcapes.reference.Reference;

import net.moddercoder.achievcapes.IAchievementCapesCore;

import net.minecraft.client.renderer.entity.RenderPlayer;

import net.minecraftforge.fml.client.registry.ClientRegistry;

import net.moddercoder.achievcapes.client.event.InputHandler;

import net.moddercoder.achievcapes.common.CommonAchievementCapesCore;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.moddercoder.achievcapes.client.renderer.EntityPlayerRenderer;

import net.moddercoder.achievcapes.client.event.ClientPlayerConnectionEvent;

@SideOnly(Side.CLIENT)
public class ClientProxy implements IProxy {
	
	public static final KeyBinding KEY_OPEN_CAPE_MENU1 = new KeyBinding("key." + Reference.MODID + ".open_menu.desc", Keyboard.KEY_V, "key." + Reference.MODID + ".category");
	
	private IAchievementCapesCore core;
    
	@Override
	public void preInit(FMLPreInitializationEvent event) {
		this.core = new CommonAchievementCapesCore();
	}
	
	@Override
	public void init(FMLInitializationEvent event) {
		final Map<String, RenderPlayer> skinMap = Minecraft.getMinecraft().getRenderManager().getSkinMap();
		
		RenderPlayer render;
		render = skinMap.get("default");
		render.addLayer(new EntityPlayerRenderer());
		
		render = skinMap.get("slim");
		render.addLayer(new EntityPlayerRenderer());
		
		//###########################################################################################
		
		MinecraftForge.EVENT_BUS.register(new InputHandler());
		MinecraftForge.EVENT_BUS.register(new ClientPlayerConnectionEvent());
		
		//###########################################################################################
		
		ClientRegistry.registerKeyBinding(ClientProxy.KEY_OPEN_CAPE_MENU1);
	}
	
	@Override
	public void postInit(FMLPostInitializationEvent event) {
		
	}
	
	@Override
	public void registerItemModel(Item item, int meta, String name) {
		ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(Reference.MODID.concat(":") + name, "inventory"));
	}
	
	@Override
	public IAchievementCapesCore getCore() {
		return this.core;
	}
}