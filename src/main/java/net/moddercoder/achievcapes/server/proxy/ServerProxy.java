package net.moddercoder.achievcapes.server.proxy;

import net.minecraft.item.Item;

import net.moddercoder.achievcapes.proxy.IProxy;

import net.moddercoder.achievcapes.IAchievementCapesCore;

import net.moddercoder.achievcapes.server.ServerAchievementCapesCore;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;

public class ServerProxy implements IProxy {
	
	public IAchievementCapesCore core;
	
	@Override
	public void preInit(FMLPreInitializationEvent event) {
		this.core = new ServerAchievementCapesCore();
	}
	
	@Override
	public void init(FMLInitializationEvent event) {
		
	}
	
	@Override
	public void postInit(FMLPostInitializationEvent event) {
		
	}
	
	@Override
	public void registerItemModel(Item item, int meta, String name) {}
	
	@Override
	public IAchievementCapesCore getCore() {
		return this.core;
	}
	
}