package net.moddercoder.achievcapes.server;

import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.UUID;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.lang.reflect.Type;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;

import java.net.URL;
import java.net.MalformedURLException;

import java.util.function.Supplier;
import java.util.function.Predicate;

import com.google.gson.reflect.TypeToken;

import com.google.gson.JsonSyntaxException;

import net.minecraft.util.ResourceLocation;

import java.util.concurrent.ConcurrentHashMap;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.entity.player.EntityPlayer;

import net.moddercoder.achievcapes.cape.ICape;
import net.moddercoder.achievcapes.cape.config.Configs;
import net.moddercoder.achievcapes.cape.CapeManager;

import net.moddercoder.achievcapes.reference.Reference;

import net.moddercoder.achievcapes.AchievementCapes;
import net.moddercoder.achievcapes.IAchievementCapesCore;

import net.moddercoder.achievcapes.cape.property.CapeProperty;
import net.moddercoder.achievcapes.cape.property.CapePropertyManager;

import net.moddercoder.achievcapes.common.capedata.loader.CapeLoader;

import net.moddercoder.achievcapes.common.capedata.CommonCapeDataSaver;

import net.moddercoder.achievcapes.common.network.PacketCapeAlertMessage;

public class ServerAchievementCapesCore implements IAchievementCapesCore {
	
	//=========================================LOGICAL-SERVER========================================
	private Map<UUID, List<ICape>> playersCapes = new ConcurrentHashMap<>();
	private Map<UUID, CapeProperty> lastPlayersProperty = new ConcurrentHashMap<>();
	private final CapeManager serverCapeManager = new CapeManager.Builder().build();
	private final CapePropertyManager serverCapePropertyManager = new CapePropertyManager();
	private ConcurrentHashMap<Long, Optional<URL>> serverVisualMap = new ConcurrentHashMap<>();
	protected final Map<UUID, CapeProperty> serverPlayersCapeProperties = new ConcurrentHashMap<>();
	//===============================================================================================
	
	@Override
	public CapeProperty setLastCapePropertyForPlayer(Side side, UUID playerUUID, CapeProperty property) {
		this.lastPlayersProperty.remove(playerUUID);
		return this.lastPlayersProperty.put(playerUUID, property);
	}
	
	@Override
	public CapeProperty getLastCapePropertyForPlayer(Side side, UUID playerUUID) {
		if (this.lastPlayersProperty.containsKey(playerUUID)) {
			return this.lastPlayersProperty.get(playerUUID);
		}
		return null;
	}
	
	@Override
	public boolean silentlyAddCapeForPlayer(Side side, UUID playerUUID, ICape cape) {
		if (cape == null | playerUUID == null | this.playersCapes.containsKey(playerUUID) == false) return false;
		
		final List<ICape> capes = this.playersCapes.get(playerUUID);
		if (capes.contains(cape)) {
			return false;
		}
		
		return capes.add(cape);
	}
	
	@Override
	public boolean silentlyAddCapeForPlayer(Side side, EntityPlayer player, ICape cape) {
		if (player == null) return false;
		return this.silentlyAddCapeForPlayer(side, player.getUniqueID(), cape);
	}
	
	@Override
	public boolean silentlyAddCapeForPlayerIf(Side side, Predicate<EntityPlayer> predicate, EntityPlayer player, ICape cape) {
		if (player == null | predicate == null) return false;
		if (predicate.test(player)) {				
			return this.silentlyAddCapeForPlayer(side, player.getUniqueID(), cape);
		}
		return false;
	}
	
	@Override
	public boolean removeCapeForPlayer(Side side, UUID playerUUID, ICape cape) {
		if (cape == null | playerUUID == null) return false;
		
		final List<ICape> capes = this.playersCapes.get(playerUUID);
		final Iterator<ICape> iterator = capes.iterator();
		
		while (iterator.hasNext()) {
			final ICape _cape = iterator.next();
			
			if (_cape.getSerialNumber() == cape.getSerialNumber()) {
				iterator.remove();
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean removeCapeForPlayer(Side side, EntityPlayer player, ICape cape) {
		if (player == null) return false;
		return this.removeCapeForPlayer(side, player.getUniqueID(), cape);
	}
	
	@Override
	public boolean removeCapeForPlayerIf(Side side, Predicate<UUID> predicate, UUID playerUUID, ICape cape) {
		if (predicate == null) return false;
		if (predicate.test(playerUUID)) {
			return this.removeCapeForPlayer(side, playerUUID, cape);
		}
		return false;
	}
	
	@Override
	public boolean removeCapeForPlayerIf(Side side, Predicate<EntityPlayer> predicate, EntityPlayer player, ICape cape) {
		if (predicate == null) return false;
		if (predicate.test(player)) {
			return this.removeCapeForPlayer(side, player, cape);
		}
		return false;
	}
	
	@Override
	public boolean addCapeForPlayer(Side side, EntityPlayer player, ICape cape) {
		final boolean state = this.silentlyAddCapeForPlayer(side, player.getUniqueID(), cape);
		
		final PacketCapeAlertMessage packet = new PacketCapeAlertMessage(player.getUniqueID(), cape.getSerialNumber());
		AchievementCapes.getInstance().network.sendToAll(packet);
		
		return state;
	}
	
	@Override
	public boolean addCapeForPlayerIf(Side side, Predicate<EntityPlayer> predicate, EntityPlayer player, ICape cape) {
		if (predicate == null) return false;
		if (predicate.test(player)) {
			return this.addCapeForPlayer(side, player, cape);
		}
		return false;
	}
	
	@Override
	public CapePropertyManager getCapePropertyManager(Side side) {
		return this.serverCapePropertyManager;
	}
	
	@Override
	public CapeProperty getCapePropertyFromPlayer(Side side, UUID playerUUID) {
		if (playerUUID == null) return null;
		return this.serverPlayersCapeProperties.containsKey(playerUUID) ? this.serverPlayersCapeProperties.get(playerUUID) : null;
	}
	
	@Override
	public CapeProperty getCapePropertyFromPlayer(Side side, Supplier<UUID> supplier) {
		return this.getCapePropertyFromPlayer(side, supplier.get());
	}
	
	@Override
	public void setCapePropertyForPlayer(Side side, UUID playerUUID, CapeProperty property) {
		if (this.serverPlayersCapeProperties.containsKey(playerUUID))
			this.serverPlayersCapeProperties.remove(playerUUID);
		this.serverPlayersCapeProperties.put(playerUUID, property);
	}
	
	@Override
	public void setCapePropertyForPlayer(Side side, UUID playerUUID, Supplier<CapeProperty> supplier) {
		if (playerUUID == null) return;
		this.setCapePropertyForPlayer(side, playerUUID, supplier.get());
	}
	
	@Override
	public void writeVisualsToFile(Side side, Map<Long, Optional<URL>> visualMap) {
		final HashMap<Long, URL> cleanVisualMap = new HashMap<Long, URL>();
		visualMap.entrySet().forEach((entry) -> {
			if (!entry.getValue().isPresent()) {
				AchievementCapes.logger.warn(entry.getKey() + " has a null or invalid URL!");
			}
			cleanVisualMap.put(entry.getKey(), entry.getValue().get());
		});
		
		try {
			if (!CommonCapeDataSaver.getSaveDir().exists()) {
				CommonCapeDataSaver.getSaveDir().mkdir();
			}
			final FileWriter writer = new FileWriter(this.getCapeVisualFile(Side.SERVER));
			writer.write(AchievementCapes.GSON.toJson(cleanVisualMap));
			writer.flush();
			writer.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	@Override
	public ConcurrentHashMap<Long, Optional<URL>> readVisualsFromFile(Side side) {
		final File capeVisualFile = this.getCapeVisualFile(Side.SERVER);
		
		if (capeVisualFile.exists()) {
			try {
				final BufferedReader reader = new BufferedReader(new FileReader(capeVisualFile));
				final StringBuilder builder = new StringBuilder();
				
				String line = "";
				
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				
				reader.close();
				
				final Type type = new TypeToken<Map<Long, URL>>(){}.getType();
				final HashMap<Long, URL> visualMap = AchievementCapes.GSON.fromJson(builder.toString(), type);
				
				final ConcurrentHashMap<Long, Optional<URL>> visualMapWithOptionals = new ConcurrentHashMap<>();
				
				if (visualMap.isEmpty()) {
					return visualMapWithOptionals;
				}
				
				visualMap.entrySet().forEach((entry) ->
					visualMapWithOptionals.put(entry.getKey(), Optional.ofNullable(entry.getValue()))
				);
				
				this.serverVisualMap = visualMapWithOptionals;
				
				return this.serverVisualMap;
			} catch (FileNotFoundException fnfe) {
				fnfe.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (final JsonSyntaxException jse) {
				final ConcurrentHashMap<Long, Optional<URL>> visualMap = this.createVisuals(side);
				this.writeVisualsToFile(Side.SERVER, visualMap);
				this.serverVisualMap = visualMap;
				return this.serverVisualMap;
			} 
		}
		
		return null;
	}
	
	@Override
	public Collection<ICape> getAvailableCapes(Side side, UUID playerUUID) {
		if (playerUUID == null) return Collections.emptyList();
		return this.playersCapes.containsKey(playerUUID) ? this.playersCapes.get(playerUUID) : null;
	}
	
	@Override
	public Collection<ICape> getAvailableCapes(Side side, EntityPlayer player) {
		if (player == null) return Collections.emptyList();
		return this.getAvailableCapes(side, player.getUniqueID());
	}
	
	@Override
	public int getAvailableCapesCount(Side side, UUID playerUUID) {
		if (playerUUID == null) return -1;
		return this.playersCapes.containsKey(playerUUID) ? this.playersCapes.get(playerUUID).size() : 0;
	}
	
	@Override
	public int getAvailableCapesCount(Side side, EntityPlayer player) {
		if (player == null) return -1;
		return this.getAvailableCapesCount(side, player.getUniqueID());
	}
	
	@Override
	public ConcurrentHashMap<Long, Optional<URL>> getVisualMap() {
		return this.serverVisualMap;
	}
	
	@Override
	public ConcurrentHashMap<Long, ResourceLocation> getClientVisualMap() {
		return new ConcurrentHashMap<>();
	}
	
	@Override
	public void initCapeData (Side side, HashMap<UUID, List<Long>> playersData) {
		final Set<UUID> uuids = playersData.keySet();
		final ConcurrentHashMap<UUID, List<ICape>> playersCapes = new ConcurrentHashMap<>();
		
		uuids.forEach((uuid) -> {
			final List<ICape> capes = new ArrayList<>();
			playersData.get(uuid).forEach((serial) -> {
				final Optional<ICape> cape = this.serverCapeManager.getCapeBySerialNumber(serial);
				cape.ifPresent(capes::add);
				
				playersCapes.put(uuid, capes);
			});
		});
		
		this.playersCapes = playersCapes;
	}
	
	@Override
	public void eraseCapeDataForPlayer(Side side, UUID playerUUID) {
		if (playerUUID == null) return;
		if (this.playersCapes.containsKey(playerUUID)) {
			this.playersCapes.remove(playerUUID);
		}
	}
	
	@Override
	public void eraseCapeDataForPlayer(Side side, EntityPlayer player) {
		if (player == null) return;
		this.eraseCapeDataForPlayer(side, player.getUniqueID());
	}
	
	@Override
	public void initCapeDataForPlayer(Side side, UUID playerUUID) {
		if (playerUUID == null) return;
		if (this.playersCapes.containsKey(playerUUID) == false) {
			this.playersCapes.put(playerUUID, new ArrayList<>());
		}
	}
	
	@Override
	public void initCapeDataForPlayer(Side side, EntityPlayer player) {
		if (player == null) return;
		this.initCapeDataForPlayer(side, player.getUniqueID());
	}
	
	@Override
	public Collection<UUID> getPlayersList(Side side) {
		return this.playersCapes.keySet();
	}
	
	@Override
	public CapeManager getCapeManager(Side side) {
		return this.serverCapeManager;
	}
	
	@Override
	public File getCapeVisualFile(Side side) {
		final File file = new File(CommonCapeDataSaver.getSaveDir().getAbsolutePath(), "." + Reference.MODID + "_visual");
		return file;
	}
	
	@Override
	public void initVisuals(Side side) {
		if (side == Side.CLIENT) {
			return;
		}
		
		final ConcurrentHashMap<Long, Optional<URL>> visualMap = new ConcurrentHashMap<>();
		final ConcurrentHashMap<Long, Optional<URL>> readedVisualMap = this.readVisualsFromFile(Side.SERVER);
		
		if (!CapeLoader.firstRun) {
			final Collection<ICape> capesWithoutDeveloper = this.getCapeManager(Side.SERVER).getAllCapesWithoutDeveloper(); 
			if (readedVisualMap == null) {
				capesWithoutDeveloper.forEach((cape) ->
					visualMap.put(cape.getSerialNumber(), this.appendCapeToVisual(cape))
				);
			} else {
				//Remove unused visuals from file
				final HashMap<Long, Optional<URL>> tempReadedVisualMap = new HashMap<>();
				for (final Map.Entry<Long, Optional<URL>> readedCapeEntry : readedVisualMap.entrySet()) {
					boolean found = false;
					for (final ICape capeSerialNumber : capesWithoutDeveloper) {
						if (capeSerialNumber.getSerialNumber() == readedCapeEntry.getKey()) {
							found = true;
							break;
						}
					}
					if (found) {
						tempReadedVisualMap.put(readedCapeEntry.getKey(), readedCapeEntry.getValue());
					} else {
						AchievementCapes.logger.info("Founded unused visual cape with serial \"" + readedCapeEntry.getKey() + "\" -> cleaned!");
					}
				}
				readedVisualMap.clear();
				readedVisualMap.putAll(tempReadedVisualMap);
				//...
				capesWithoutDeveloper.forEach((cape) -> {
					if (readedVisualMap.containsKey(cape.getSerialNumber())) {
						Optional<URL> url = readedVisualMap.get(cape.getSerialNumber());
						if (Configs.textureValidation) {
							if (url.isPresent()) {
								AchievementCapes.logger.info(String.format("Validation texture of \"%s\" cape: [\"%s\"]", cape.getName(), url.get()));
								if (!AchievementCapes.IMGUR_MANAGER.validateURL(url.get())) {
									AchievementCapes.logger.warn(String.format("URL: \"%s\", for cape: \"%s\" is invalid. Texture will be reuploaded!", url.get(), cape.getName()));
									url = this.appendCapeToVisual(cape);
								}
							}
						}
						visualMap.put(cape.getSerialNumber(), (url.isPresent() ? url : Optional.<URL>empty()));
					} else {
						AchievementCapes.logger.info(String.format("Uploading texture for new cape: \"%s\"...", cape.getName()));
						visualMap.put(cape.getSerialNumber(), this.appendCapeToVisual(cape));
					}
				});
			}
		} else {
			visualMap.putAll(this.createVisuals(side));
		}
		
		this.writeVisualsToFile(Side.SERVER, visualMap);
		this.serverVisualMap = visualMap;
	}
	
	private ConcurrentHashMap<Long, Optional<URL>> createVisuals(final Side side) {
		final ConcurrentHashMap<Long, Optional<URL>> visualMap = new ConcurrentHashMap<>();
		this.getCapeManager(side).getAllCapesWithoutDeveloper().forEach((cape) -> {
			try {
				final URL textureURL = new URL(CapeLoader.defaultCapesTextures.get(cape.getSerialNumber()));
				if (Configs.textureValidation) {
					AchievementCapes.logger.info(String.format("Validation texture of \"%s\" cape: [\"%s\"]", cape.getName(), textureURL));
					if (AchievementCapes.IMGUR_MANAGER.validateURL(textureURL)) {
						visualMap.put(cape.getSerialNumber(), Optional.of(textureURL));
					} else {
						AchievementCapes.logger.warn(String.format("URL: \"%s\", for cape: \"%s\" is invalid. Texture will be reuploaded!", textureURL, cape.getName()));
						visualMap.put(cape.getSerialNumber(), this.appendCapeToVisual(cape));
					}
				} else {
					visualMap.put(cape.getSerialNumber(), Optional.of(textureURL));
				}
			} catch (MalformedURLException me) {}
		});
		return visualMap;
	}
	
	private Optional<URL> appendCapeToVisual(ICape cape) {
		Optional<URL> url = Optional.<URL>empty();
		if (CapeLoader.isCapeTextureExists(cape)) {
			final BufferedImage image = CapeLoader.readCapeTexture(cape);
			url = Optional.ofNullable(AchievementCapes.IMGUR_MANAGER.uploadImage(image));
		} else {
			for (int i = 0; i < 3; i ++) {
				AchievementCapes.logger.warn(String.format("Unable to find cape.png for: \"%s\"", cape.getName()));
			}
		}
		return url;
	}
	
	@Override
	public void resetAll(Side side) {
		this.playersCapes.clear();
		this.serverVisualMap.clear();
		this.serverCapeManager.clear();
		this.serverPlayersCapeProperties.clear();
	}
}