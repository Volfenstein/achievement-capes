package net.moddercoder.achievcapes;

import java.io.File;

import java.util.List;
import java.util.UUID;
import java.util.HashMap;

import net.minecraft.item.Item;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.logging.log4j.Logger;

import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

import net.minecraftforge.event.RegistryEvent;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraftforge.common.MinecraftForge;

import net.moddercoder.achievcapes.proxy.IProxy;

import net.moddercoder.achievcapes.init.ModItems;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

import net.moddercoder.achievcapes.util.ImgurManager;
import net.moddercoder.achievcapes.util.NetworkUtils;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.Mod.EventHandler;

import net.moddercoder.achievcapes.reference.Reference;

import net.moddercoder.achievcapes.cape.config.Configs;

import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraftforge.fml.common.network.NetworkRegistry;

import net.moddercoder.achievcapes.cape.property.CapeProperty;

import net.minecraftforge.event.entity.player.AdvancementEvent;

import net.moddercoder.achievcapes.common.cape.CommonCapesList;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;

import net.moddercoder.achievcapes.common.event.PlayerConnectionEvent;

import net.moddercoder.achievcapes.common.capedata.CommonCapeDataSaver;
import net.moddercoder.achievcapes.common.capedata.loader.CapeLoader;
import net.moddercoder.achievcapes.advancement.AchievementCapesAdvancement;

import net.moddercoder.achievcapes.cape.property.vars.BooleanPropertyVarType;

import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;

import net.moddercoder.achievcapes.client.network.PacketSelectCape;
import net.moddercoder.achievcapes.client.network.PacketTakeOffCape;
import net.moddercoder.achievcapes.client.network.PacketGetCapesList;
import net.moddercoder.achievcapes.client.network.PacketGetCapesVisuals;
import net.moddercoder.achievcapes.client.network.PacketGetAvailablesCapesList;
import net.moddercoder.achievcapes.client.network.PacketRequestSyncPlayerCapes;

import net.moddercoder.achievcapes.common.network.PacketSendCapesList;
import net.moddercoder.achievcapes.common.network.PacketSendCapesVisuals;
import net.moddercoder.achievcapes.common.network.PacketCapeAlertMessage;
import net.moddercoder.achievcapes.common.network.PacketChangeCapeProperty;
import net.moddercoder.achievcapes.common.network.PacketSelectCapeValidate;
import net.moddercoder.achievcapes.common.network.PacketSendAvailablesCapes;
import net.moddercoder.achievcapes.common.network.PacketTakeOffCapeValidate;
import net.moddercoder.achievcapes.common.network.PacketInternetConnectionInfo;
import net.moddercoder.achievcapes.common.network.PacketSyncPlayerCapeProperties;

@Mod(modid = Reference.MODID, name = Reference.NAME, version = Reference.VERSION)
public class AchievementCapes {
	
	@SidedProxy(modId = Reference.MODID, clientSide = Reference.CLIENT_PROXY, serverSide = Reference.SERVER_PROXY)
	public static IProxy proxy;
	
	@Mod.Instance
	private static AchievementCapes instance;
	
	public static AchievementCapes getInstance() {
		return instance;
	}
	
	public static Logger logger;
	
	public static boolean hasInternetConnection = true;
	
	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	public static final ImgurManager IMGUR_MANAGER = new ImgurManager.Builder().build();
    
	public static double offsetX = 0d;
	public static double offsetY = 0d;
	public static double offsetZ = 0d;
	
    public static IAchievementCapesCore getCore () {
    	return AchievementCapes.proxy.getCore();
    }
	
	//======================================================================================================
	
    public SimpleNetworkWrapper network;
    
	@EventHandler
	public void serverStarting (final FMLServerStartingEvent event) throws Exception {
		AchievementCapes.hasInternetConnection = NetworkUtils.hasInternetConnection();
		if (AchievementCapes.hasInternetConnection == false) {
			return;
		}
		
		CommonCapesList.loadAndRegisterFromWorld();
		AchievementCapes.getCore().initVisuals(Side.SERVER);
		
		final HashMap<UUID, List<Long>> playersData = CommonCapeDataSaver.readData();
		if (playersData != null) {
			AchievementCapes.getCore().initCapeData(Side.SERVER, playersData);
		}
	}
    
    @EventHandler
    public void serverStopping(final FMLServerStoppingEvent event) {
    	CapeLoader.firstRun = false;
    	CommonCapeDataSaver.saveAllPlayersData();
    	AchievementCapes.getCore().resetAll(Side.SERVER);
    }
    
    @EventHandler
    public void postInit(final FMLPostInitializationEvent event) {
    	proxy.postInit(event);
    }
	
    @EventHandler
    public void preInit(final FMLPreInitializationEvent event) throws Exception {
    	Configs.init(event.getModConfigurationDirectory().getAbsolutePath() + File.separator + Reference.MODID + ".cfg");
    	
    	proxy.preInit(event);
    	
    	logger = event.getModLog();
    	
    	CapeProperty.CapePropertySerializer.addCapePropertyVarType(new BooleanPropertyVarType("hidden", false));
    	
    	network = NetworkRegistry.INSTANCE.newSimpleChannel(Reference.MODID);
		network.registerMessage(PacketGetAvailablesCapesList.class, PacketGetAvailablesCapesList.class, 0, Side.SERVER);
		network.registerMessage(PacketSendAvailablesCapes.class, PacketSendAvailablesCapes.class, 1, Side.CLIENT);
		network.registerMessage(PacketSelectCape.class, PacketSelectCape.class, 2, Side.SERVER);
		network.registerMessage(PacketSelectCapeValidate.class, PacketSelectCapeValidate.class, 3, Side.CLIENT);
		network.registerMessage(PacketTakeOffCape.class, PacketTakeOffCape.class, 4, Side.SERVER);
		network.registerMessage(PacketTakeOffCapeValidate.class, PacketTakeOffCapeValidate.class, 5, Side.CLIENT);
		network.registerMessage(PacketRequestSyncPlayerCapes.class, PacketRequestSyncPlayerCapes.class, 6, Side.SERVER);
        network.registerMessage(PacketSyncPlayerCapeProperties.class, PacketSyncPlayerCapeProperties.class, 7, Side.CLIENT);
        network.registerMessage(PacketChangeCapeProperty.class, PacketChangeCapeProperty.class, 8, Side.CLIENT);
        network.registerMessage(PacketSendCapesList.class, PacketSendCapesList.class, 9, Side.CLIENT);
		network.registerMessage(PacketGetCapesList.class, PacketGetCapesList.class, 10, Side.SERVER);
		network.registerMessage(PacketGetCapesVisuals.class, PacketGetCapesVisuals.class, 11, Side.SERVER);
		network.registerMessage(PacketSendCapesVisuals.class, PacketSendCapesVisuals.class, 12, Side.CLIENT);
		network.registerMessage(PacketCapeAlertMessage.class, PacketCapeAlertMessage.class, 13, Side.CLIENT);
		network.registerMessage(PacketInternetConnectionInfo.class, PacketInternetConnectionInfo.class, 14, Side.CLIENT);
    }
    
    @EventHandler
    public void init(final FMLInitializationEvent event) {
    	proxy.init(event);
    	
    	MinecraftForge.EVENT_BUS.register(new PlayerConnectionEvent());
    }
    
    @Mod.EventBusSubscriber
    public static class RegisterHandler {
    	@SubscribeEvent
    	public static void registerItem(RegistryEvent.Register<Item> event) {
    		ModItems.registerItems(event);
    	}
    	@SubscribeEvent
    	public static void registerItemModel(ModelRegistryEvent event) {
    		ModItems.registerModels(event);
    	}
    	@SubscribeEvent
    	public static void advancementEvent(AdvancementEvent event) {
    		final EntityPlayer player = event.getEntityPlayer();
    		final World world = player.getEntityWorld();
    		if (world instanceof WorldServer && player instanceof EntityPlayerMP) {
    			AchievementCapesAdvancement.checkForAdvancement(event.getAdvancement(), (WorldServer)world, (EntityPlayerMP)player);
    		}
    	}
    }
}
