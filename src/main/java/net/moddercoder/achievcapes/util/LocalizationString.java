package net.moddercoder.achievcapes.util;

import net.minecraft.client.resources.I18n;

import net.moddercoder.achievcapes.reference.Reference;

public class LocalizationString {
	
	//Returns a next string -> cape.cape_name.modid.name
	public static String ofCapeName(String name) {
		return I18n.format("cape.".concat(name + "." + Reference.MODID) + ".name");
	}
	
	public static String ofText(String name, Object ... objects) {
		return I18n.format("text.".concat(name + "." + Reference.MODID) + ".name", objects);
	}
	
	public static String ofText(String name) {
		return I18n.format("text.".concat(name + "." + Reference.MODID) + ".name");
	}
	
}
