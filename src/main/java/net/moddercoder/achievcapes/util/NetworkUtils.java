package net.moddercoder.achievcapes.util;

import java.net.URL;
import java.net.HttpURLConnection;

public class NetworkUtils {
	
	public static final boolean hasInternetConnection() {
		try {
			final HttpURLConnection httpConnection = (HttpURLConnection)(new URL("https://api.imgur.com/3/image").openConnection());
			httpConnection.connect();
			return true;
		} catch (Exception exception) {
			return false;
		}
	}
	
}