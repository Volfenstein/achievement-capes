package net.moddercoder.achievcapes.util;

import net.minecraft.util.ResourceLocation;

import net.moddercoder.achievcapes.reference.Reference;

public class ResourceLocations {
	
	public static final ResourceLocation ofCapeTexture (final String NAME) {
		return new ResourceLocation(Reference.MODID.concat(":textures/capes/" + NAME + "/cape.png"));
	}
	
	public static final ResourceLocation ofWithSplit (final String DATA) {
		final String[] _DATA = DATA.split(":");
		final String MODID = _DATA[0] == null ? "minecraft" : _DATA[0];
		final String NAME = _DATA[1] == null ? "story/mine_stone" : _DATA[1];
		return ResourceLocations.of(MODID, NAME);
	}
	
	public static final ResourceLocation of (final String MODID, final String NAME) {
		return new ResourceLocation(MODID, NAME);
	}
	
	public static final ResourceLocation of (final String NAME) {
		return new ResourceLocation(NAME);
	}
	
}
