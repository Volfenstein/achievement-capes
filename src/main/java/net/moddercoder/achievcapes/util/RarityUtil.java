package net.moddercoder.achievcapes.util;

import net.minecraft.item.EnumRarity;

import net.minecraft.util.text.TextFormatting;

public class RarityUtil {
	
	public static int getColorByRarity(EnumRarity rarity) {
		switch(rarity) {
			case UNCOMMON:
				return 0xffff55;
			case RARE:
				return 0x55ffff;
			case EPIC:
				return 0xff55ff;
			default: break;
		}
		return 0xffffff;
	}
	
	public static TextFormatting getFormattingByRarity(EnumRarity rarity) {
		switch(rarity) {
			case UNCOMMON:
				return TextFormatting.YELLOW;
			case RARE:
				return TextFormatting.AQUA;
			case EPIC:
				return TextFormatting.LIGHT_PURPLE;
			default:
				return TextFormatting.WHITE;
		}
	}
	
	public static EnumRarity getRarityByName (String name) {
		switch(name) {
			case "COMMON":
				return EnumRarity.COMMON;
			case "UNCOMMON":
				return EnumRarity.UNCOMMON;
			case "RARE":
				return EnumRarity.RARE;
			case "EPIC":
				return EnumRarity.EPIC;
			default:
				return EnumRarity.COMMON;
		}
	}
	
}