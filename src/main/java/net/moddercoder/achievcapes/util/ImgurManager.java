package net.moddercoder.achievcapes.util;

import java.util.Base64;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.awt.image.BufferedImage;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.ByteArrayOutputStream;

import java.net.URL;
import java.net.URLEncoder;
import java.net.HttpURLConnection;
import java.net.UnknownHostException;
import java.net.MalformedURLException;

import net.moddercoder.achievcapes.AchievementCapes;
import net.moddercoder.achievcapes.reference.Reference;

//
// Thanks to https://github.com/DarkEyeDragon/ScreenshotUploader
//

public class ImgurManager {
	
	private ImgurManager() {}
	
	public URL uploadImage(final BufferedImage image) {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		int responseCode = -1;
		
		try {
			final URL url = new URL("https://api.imgur.com/3/image");
			final HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
			httpConnection.setDoInput(true);
			httpConnection.setDoOutput(true);
			
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Authorization", "Client-ID 546c25a59c58ad7");
			httpConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			httpConnection.connect();
			
			ImageIO.write(image, "png", baos);
			baos.flush();
			
			final byte[] bytes = baos.toByteArray();
			String encoded = Base64.getEncoder().encodeToString(bytes);
			
			final OutputStreamWriter streamWriter = new OutputStreamWriter(httpConnection.getOutputStream());
			String data = URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(encoded, "UTF-8");
			streamWriter.write(data);
			streamWriter.flush();
			
			String line;
			final StringBuilder builder = new StringBuilder();
			final BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
			while ((line = reader.readLine()) != null) {
				builder.append(line).append("\n");
			}
			
			responseCode = httpConnection.getResponseCode();
			streamWriter.close();
			reader.close();
			
			final JsonObject jsonObject = new JsonParser().parse(builder.toString()).getAsJsonObject();
			final String result = jsonObject.get("data").getAsJsonObject().get("link").getAsString();
			
			AchievementCapes.logger.info("[" + responseCode + "] Uploaded cape texture to: \"" + result + "\"");
			return new URL(result);
		} catch (final MalformedURLException murle) {
			AchievementCapes.logger.warn("[" + responseCode + "] Unable to upload texture");
			murle.printStackTrace();
		} catch (final IOException ioe) {
			AchievementCapes.logger.warn("[" + responseCode + "] Unable to upload texture");
			ioe.printStackTrace();
		}
		
		return null;
	}
	
	public BufferedImage downloadImage(final URL url) {
		try {
			final HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setRequestProperty("Host", "i.imgur.com");
			urlConnection.setRequestProperty("User-Agent", Reference.MODID + "/" + Reference.VERSION);
			urlConnection.setRequestProperty("Accept", "image/png;image/jpg;image/jpeg");
			urlConnection.connect();
			
			final BufferedImage image = ImageIO.read(urlConnection.getInputStream());
			
			if (image != null && (image.getWidth() == 22 & image.getHeight() == 17)) {
				return image;
			}
			if (image == null) {
				AchievementCapes.logger.error("Unable to load image from url: " + url);
			} else if (image.getWidth() != 22 || image.getHeight() != 17) {
				AchievementCapes.logger.error("Image of cape has invalid size [should be 22x17] got [" + image.getWidth() + "x" + image.getHeight() + "]: " + url);				
			}
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		}
		
		return new BufferedImage(22, 17, BufferedImage.TYPE_INT_RGB);
	}
	
	public boolean validateURL(final URL url) {
		if (url == null) return false;
		try {
			final HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
			return httpConnection.getResponseCode() == 200;
		} catch (final UnknownHostException uhe) {
			return false;
		} catch (final IOException ioe) {
			ioe.printStackTrace();
			return false;
		}
	}
	
	public static class Builder {
		public ImgurManager build() {
			return new ImgurManager();
		}
	}
}