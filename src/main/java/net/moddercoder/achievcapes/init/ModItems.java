package net.moddercoder.achievcapes.init;

import net.minecraft.item.Item;

import net.minecraftforge.event.RegistryEvent;

import net.minecraftforge.registries.IForgeRegistry;

import net.moddercoder.achievcapes.item.CapePatternItem;

import net.minecraftforge.client.event.ModelRegistryEvent;

public class ModItems {
	
	public static final CapePatternItem CAPE_PATTERN_ITEM = new CapePatternItem();
	
	public static final void registerItems(RegistryEvent.Register<Item> event) {
		final IForgeRegistry<Item> REGISTRY = event.getRegistry(); 
		
		REGISTRY.register(CAPE_PATTERN_ITEM);
	}
	
	public static final void registerModels(ModelRegistryEvent event) {
		CAPE_PATTERN_ITEM.registerItemModel();
	}
	
}
