package net.moddercoder.achievcapes.item;

import java.util.Collection;

import net.minecraft.world.World;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import net.minecraft.util.EnumHand;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.creativetab.CreativeTabs;

import net.moddercoder.achievcapes.cape.ICape;

import net.minecraft.entity.player.EntityPlayer;

import net.moddercoder.achievcapes.cape.CapesList;
import net.moddercoder.achievcapes.cape.CapeManager;

import net.moddercoder.achievcapes.reference.Reference;

import net.moddercoder.achievcapes.AchievementCapes;
import net.moddercoder.achievcapes.IAchievementCapesCore;

public class CapePatternItem extends Item implements IBasicItem {
	
	public CapePatternItem () {
		this.setCreativeTab(CreativeTabs.MISC);
		this.setRegistryName("cape_pattern_item");
		this.setUnlocalizedName(Reference.MODID.concat(".") + "cape_pattern_item");
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
		final ItemStack handStack = player.getHeldItem(hand);
		
		if (world.isRemote) {
			return ActionResult.newResult(EnumActionResult.SUCCESS, handStack);
		} else {
			final IAchievementCapesCore achievementCapeCore = AchievementCapes.getCore();
			
			final Collection<ICape> capes = achievementCapeCore.getAvailableCapes(Side.SERVER, player.getUniqueID());
			final CapeManager capeManager = achievementCapeCore.getCapeManager(Side.SERVER);
			
			player.getCooldownTracker().setCooldown(this, 10);
			
			if (capes.isEmpty()) {
				capeManager.getAllCapes().forEach((cape) ->
					this.addCapeForPlayer(achievementCapeCore, cape, player, player.isSneaking())
				);
				
				if (player.getUniqueID().equals(Reference.DEVELOPER_UUID)) {
					this.addCapeForPlayer(achievementCapeCore, CapesList.DEVELOPER_CAPE, player, player.isSneaking());
				}
				
				return ActionResult.newResult(EnumActionResult.SUCCESS, handStack);
			}
			
			capeManager.getAllCapes().forEach((cape) -> AchievementCapes.getCore().removeCapeForPlayer(Side.SERVER, player.getUniqueID(), cape));
			return ActionResult.newResult(EnumActionResult.SUCCESS, handStack);
		}
	}
	
	private void addCapeForPlayer(final IAchievementCapesCore achievementCapeCore, final ICape cape, final EntityPlayer player, final boolean silently) {
		if (silently) {
			achievementCapeCore.silentlyAddCapeForPlayer(Side.SERVER, player, cape);						
		} else {
			achievementCapeCore.addCapeForPlayer(Side.SERVER, player, cape);
		}
	}
	
	@Override
	public void registerItemModel() {
		AchievementCapes.proxy.registerItemModel(this, 0, "cape_pattern_item");
	}
	
}