package net.moddercoder.achievcapes.advancement;

import java.util.Iterator;
import java.util.Collection;

import net.minecraft.world.WorldServer;

import net.minecraft.util.ResourceLocation;

import net.minecraft.advancements.Advancement;

import net.moddercoder.achievcapes.cape.ICape;

import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

import net.moddercoder.achievcapes.AchievementCapes;

public class AchievementCapesAdvancement {
	
	public static void checkForAdvancement (ResourceLocation advancementID, WorldServer world, EntityPlayerMP player) {
		AchievementCapes.getCore().getCapeManager(Side.SERVER).getAllCapes().forEach((cape) -> {
			final ResourceLocation capeAdvancementID = cape.getAdvancementID();
			if (capeAdvancementID != null) {
				if (capeAdvancementID.equals(advancementID)) {
					AchievementCapes.getCore().addCapeForPlayer(Side.SERVER, player, cape);
					return;
				}
			}
		});
	}
	
	public static void checkForAdvancement (Advancement advancement, WorldServer world, EntityPlayerMP player) {
		AchievementCapesAdvancement.checkForAdvancement(advancement.getId(), world, player);
	}
	
	public static void checkForAdvancements (final WorldServer world, final EntityPlayerMP player) {
		final Iterator<Advancement> advancements = world.getAdvancementManager().getAdvancements().iterator();
		
		while(advancements.hasNext()) {
			final Advancement advancement = advancements.next();
			final Collection<ICape> playerAvailableCapes = AchievementCapes.getCore().getAvailableCapes(Side.SERVER, player);
			AchievementCapes.getCore().getCapeManager(Side.SERVER).getAllCapes().forEach((cape) -> {
				if (!playerAvailableCapes.contains(cape)) {
					final ResourceLocation capeAdvancementID = cape.getAdvancementID();
					if (capeAdvancementID != null) {
						if (capeAdvancementID.equals(advancement.getId())) {
							if (player.getAdvancements().getProgress(advancement).isDone()) {
								AchievementCapes.getCore().addCapeForPlayer(Side.SERVER, (EntityPlayer)player, cape);
							}
						}
					}
				}
			});
		}
	}
	
}